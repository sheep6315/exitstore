//
//  Configs.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

#if Dev //開發測試
let SERVER_HOST = "https://drssit.tstartel.com/DRS/ws"
let VERSION_CODE = 0
#elseif Uat //客戶測試
let SERVER_HOST = "http://WEB_SERVER_IP:8080/DRSAPPWEB/ws/uat"
let VERSION_CODE = 0
#elseif Prod //正式
let SERVER_HOST = "http://WEB_SERVER_IP:8080/DRSAPPWEB/ws/prod"
let VERSION_CODE = 0
#else
let SERVER_HOST = "http://WEB_SERVER_IP:8080/DRSAPPWEB/ws"
let VERSION_CODE = 0
#endif

let VERSION_STATUS = "VERSION_STATUS"

enum VersionStatus: Int {
    case forceUpdate = 0
    case remindUpdate
    case notReleased
    case noUpdateStatus
}

/// API超時時間
let API_TIMEOUT_INTERVAL: Double = 30

/// 預設status bar樣式
let DEFAULT_STATUS_BAR_STYLE = UIStatusBarStyle.lightContent

/// 預設status bar顏色
let DEFAULT_STATUS_BAR_COLOR = UIColor.black

/// 預設navigation bar背景顏色
let DEFAULT_NAVIGATION_BAR_BACKGROUND_COLOR = UIColor.white

let DEFAULT_UI_COLOR = UIColor(red: 163/255, green: 138/255, blue: 233/255, alpha: 1)

/// 預設navigation bar元件顏色
let DEFAULT_NAVIGATION_BAR_TITLE_COLOR = UIColor(red: 135/255, green: 40/255, blue: 110/255, alpha: 1)

let DATEFORMATTER: String = "yyyy-MM-dd HH:mm:ss"

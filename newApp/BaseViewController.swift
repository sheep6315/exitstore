//
//  BaseViewController.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

/// 基礎共用ViewController
class BaseViewController: UIViewController,UIPopoverPresentationControllerDelegate,UIScrollViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //navigationBar設置
        //返回鍵設定
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationItem.backBarButtonItem = backButton
//        //右方按鈕
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_menu")!.tint(with: UIColor.red), style: .plain, target: self, action: #selector(didTapSideMenuButton))
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.view.backgroundColor = UIColor.black
        //中間logo
        let logo = UIImage(named: "img_logo_black")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        self.navigationItem.titleView?.sizeToFit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NetworkUtiltiy.cancelAllTask(vc: self, priorityLowerThan: URLSessionTask.highPriority)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - @IBAction
    /// 點擊漢堡按鈕
    @objc func didTapSideMenuButton() {
        print("didTapSideMenuButton")
    }
    
    //MARK: - custom func
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

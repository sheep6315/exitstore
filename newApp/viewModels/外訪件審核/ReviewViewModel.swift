//
//  ReviewViewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/16.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

protocol ReviewViewModelDelegate: ViewModelDelegate {
    /// 取得紀錄成功
    func onGetReviewListSuccess()
    /// 取得紀錄失敗
    func onGetReviewListFail(message: String)
    
    /// 審核成功
    func onReviewSuccess(message: String)
    /// 審核失敗
    func onReviewFail(message: String)
}

class ReviewViewModel: NSObject,ViewModel {
    
    var delegate: ReviewViewModelDelegate?
    
    private var reviewModelList:ReviewModelList = ReviewModelList.shared!
    
    private let reasonList = ReasonModelList.shared!.list
    
    private let userModel = UserModel.shared!
    
    /// 外訪件列表
    var list:[ReviewModel] {
        return reviewModelList.list
    }
    
    override init() {
        super.init()
        
    }
    
    func getReason(id:String) -> String {
        for i in 0..<reasonList.count {
            if reasonList[i].reason_id == id {
                return reasonList[i].reason_name
            }
        }
        return ""
    }
    
    /// 取得外訪件清單
    func getReviewList() {
        reviewModelList.getList(empyId: userModel.empId,showProgress: true, completion: { (result, statusCode,message) in
            self.delegate?.reloadView()
            if statusCode == .success {
                self.delegate?.onGetReviewListSuccess()
            }else {
                self.delegate?.onGetReviewListFail(message: message)
            }
        }) { (error) in
            self.delegate?.reloadView()
            self.delegate?.onGetReviewListFail(message: error.localizedDescription)
        }
    }
    
    func actionApproval(row:Int) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATEFORMATTER
        
        reviewModelList.list[row].approval(empyId: userModel.empId, apptime: dateFormatter.string(from: Date()), approver: userModel.cname, showProgress: true, completion: { (result, statusCode, message) in
            if statusCode == .success {
                self.getReviewList()
                self.delegate?.onReviewSuccess(message: message)
            }else {
                self.delegate?.onReviewFail(message: message)
            }
        }) { (error) in
            self.delegate?.onReviewFail(message: error.localizedDescription)
        }
    }
}


//
//  ViewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

/// ViewModelDelegate
protocol ViewModelDelegate {
    
    /// 重新整理頁面內容
    func reloadView()
}

/// ViewModel
protocol ViewModel {
    
    associatedtype delegateType
    
    var delegate: delegateType? { get set }
}

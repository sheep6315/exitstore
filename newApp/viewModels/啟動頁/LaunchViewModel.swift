//
//  LaunchViewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

/// LaunchViewModelDelegate
protocol LaunchViewModelDelegate: ViewModelDelegate {
    func onGetReasonTypeSuccess()
    func onGetReasonTypeFail()
    /// 取得版本資訊 成功
    ///
    /// - Parameter message:
    func onGetAppVersionSuccess()
    
    /// 取得版本資訊 失敗
    ///
    /// - Parameter message:
    func onGetAppVersionFail(message: String)
}

/// LaunchViewModel
class LaunchViewModel: NSObject, ViewModel {
    
    var delegate: LaunchViewModelDelegate?
    
    private let reasonModelList = ReasonModelList.shared!
    
    private let userModel = UserModel.shared!
    
    private var versionModel = VersionModel.shared!
    
    /// 最新版號
    var vid: String {
        return versionModel.curVer
    }
    
    var title:String {
        return versionModel.title
    }
    
    /// 是否顯示提示
    var alert:Bool {
        return true
    }
    
    var curVer:String {
        return versionModel.curVer
    }
    
    var curVerCode:Int {
        return Int(versionModel.curVerCode) ?? 0
    }
    
    var minVer:String {
        return versionModel.minVer
    }
    
    var minVerCode:Int {
        return Int(versionModel.minVerCode) ?? 0
    }
    
    var message:String {
        return versionModel.message
    }
    
    /// 是否強制更新
    var isAppUpdate:Bool {
        return versionModel.alert
    }
    
    /// AppStore下載網址
    var updateUrl:URL? {
        return URL(string: versionModel.memo1)
    }
    
    /// 更新資訊
    var releaseNote: String {
        return versionModel.message
    }
    
    override init() {
        super.init()
        
    }
    
    func getAppVersion() {
        versionModel.getAppVersion(showProgress: true, completion: { (result, statusCode) in
            if statusCode == .success {
                self.delegate?.onGetAppVersionSuccess()
            }else {
                self.delegate?.onGetAppVersionFail(message: "onGetAppVersionFail")
            }
            self.delegate?.reloadView()
        }) { (error) in
            self.delegate?.onGetAppVersionFail(message: error.localizedDescription)
            self.delegate?.reloadView()
        }
    }
    
    /// 取得離店原因列表
    func getReasonList() {
        reasonModelList.getReasonList(empyId: userModel.empId,showProgress: true, completion: { (result, statusCode, message) in
            if statusCode == .success {
                self.delegate?.onGetReasonTypeSuccess()
            }else {
                self.delegate?.onGetReasonTypeFail()
            }
        }) { (error) in
            self.delegate?.onGetReasonTypeFail()
        }
    }
}

//
//  LoginViewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/12.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

protocol LoginViewModelDelegate: ViewModelDelegate {
    func onLoginSuccess()
    func onLoginFail(message:String)
}

let AUTOLOGINDIC = "AUTOLOGINDIC"

let ISAUTOLOGIN = "ISAUTOLOGIN"

/// 帳號的角色
///
/// - businessRepresentative: 業代
/// - assistantManager: 副店
/// - manager: 店長
/// - supervision: 督導
/// - headquarters: 總部
enum UserCharacter:String {
    case businessRepresentative = "業代"
    case assistantManager = "副店"
    case manager = "店長"
    case supervision = "督導"
    case headquarters = "總部"
    
    init(fromRawValue: String){
        self = UserCharacter(rawValue: fromRawValue) ?? .businessRepresentative
    }
}

/// 登入頁ViewModel
class LoginViewModel: NSObject,ViewModel {
    
    var delegate: LoginViewModelDelegate?
    
    private var userModel:UserModel = UserModel()
    
    /// 帳號角色
    var character:UserCharacter? {
        return UserCharacter(rawValue: UserModel.shared!.titlename)
    }
    
    override init() {
        super.init()
        
    }
    
    /// 門號登入
    ///
    /// - Parameters:
    ///   - msisdn: 門號
    ///   - password: 密碼
    func actionLogin(msisdn:String,password:String) {
        UserDefaults.standard.removeObject(forKey: API_CACHED_DATA)
        self.userModel.login(msisdn: msisdn, password: password, completion: { (result,statusCode,message) in
            if statusCode == .success {
                if let shared = UserModel.shared {
                    shared.setAttributes(result)
                    UserDefaults.standard.set(["ad_account":msisdn,"password":password], forKey: AUTOLOGINDIC)
                    UserDefaults.standard.set(true, forKey: ISAUTOLOGIN)
                }
                self.delegate?.onLoginSuccess()
            }else {
                self.delegate?.onLoginFail(message: message)
            }
        }) { (error) in
            self.delegate?.onLoginFail(message: error.localizedDescription)
        }
    }
}

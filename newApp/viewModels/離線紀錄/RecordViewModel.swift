//
//  RecordViewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/16.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

protocol RecordViewModelDelegate: ViewModelDelegate {
    /// 取得紀錄成功
    func onGetRecordListSuccess()
    /// 取得紀錄失敗
    func onGetRecordListFail()
}

class RecordViewModel: NSObject,ViewModel {
    
    var delegate: RecordViewModelDelegate?
    
    private var recordModelList:RecordModelList = RecordModelList.shared!
    
    private let userModel = UserModel.shared!
    
    /// 離店紀錄列表
    var list:[RecordModel] {
        return recordModelList.list
    }
    
    /// 離店人員
    var exitAmount:String {
        return recordModelList.exitAmount
    }
    
    /// 離店警示人員
    var alertAmount:String {
        return recordModelList.alertAmount
    }
    
    override init() {
        super.init()
        
    }
    
    /// 取得離店紀錄清單
    func getList(query_date:String = "") {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATEFORMATTER
        
        recordModelList.getList(empyId: userModel.empId,query_date: dateFormatter.string(from: Date()),showProgress: true, completion: { (result, statusCode,message) in
            self.delegate?.reloadView()
            if statusCode == .success {
                self.delegate?.onGetRecordListSuccess()
            }else {
                self.delegate?.onGetRecordListFail()
            }
        }) { (error) in
            self.delegate?.reloadView()
            self.delegate?.onGetRecordListFail()
        }
    }
}

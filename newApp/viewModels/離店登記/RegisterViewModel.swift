//
//  RegisterViewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/17.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

protocol RegisterViewModelDelegate: ViewModelDelegate {

    /// 取得預約登記清單失敗
    ///
    /// - Parameter message: 失敗訊息
    func onReservationListFail(message: String)
    
    /// 刪除預約登記成功
    ///
    /// - Parameter message: 成功訊息
    func deleteReservationSuccess(message: String)
    /// 刪除預約登記失敗
    ///
    /// - Parameter message: 失敗訊息
    func deleteReservationFail(message: String)
}

class RegisterViewModel: NSObject,ViewModel {
    
    var delegate: RegisterViewModelDelegate?
    
    private let drModelList = DRModelList.shared!
    
    private let userModel = UserModel.shared!
    
    /// 離店清單
    var list:[DRModel] {
        return drModelList.list
    }
    
    /// 狀態清單
    var statusList:[DRStatus] {
        var slist:[DRStatus] = []
        self.list.forEach { (model) in
            slist.append(model.status)
        }
        return slist
    }
    
    override init() {
        super.init()
    }

    //MARK: - custom func
    /// 設定刪除功能
    ///
    /// - Parameter row: 列表第幾列
    /// - Returns:
    func setDelete(row:Int) -> Bool {
        if list[row].status.hashValue < 2 {
            return true
        }
        return false
    }
    
    //MARK: - api
    /// 取得離店清單
    func getDRList() {
        drModelList.getList(empyId: userModel.empId,showProgress: true, completion: { (result, statusCode, message) in
            self.delegate?.reloadView()
            if statusCode == .fail {
                self.delegate?.onReservationListFail(message: message)
            }
        }) { (error) in
            self.delegate?.reloadView()
            self.delegate?.onReservationListFail(message: error.localizedDescription)
        }
    }
    
    /// 刪除預約清單
    ///
    /// - Parameter row: row
    func deleteReservationList(row:Int) {
        list[row].delete(showProgress: true, completion: { (result, statusCode, message) in
            if statusCode == .success {
                self.getDRList()
                self.delegate?.deleteReservationSuccess(message: "刪除成功")
            }else {
                self.delegate?.deleteReservationFail(message: message)
            }
        }) { (error) in
            self.delegate?.deleteReservationFail(message: error.localizedDescription)
        }
    }
}

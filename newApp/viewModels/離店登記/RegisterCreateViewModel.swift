//
//  RegisterCreateViewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/17.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit
import CoreLocation
import EventKit

protocol RegisterCreateViewModelDelegate: ViewModelDelegate {
    func onCheckInSuccess(message: String)
    func onCheckInFail(message: String)
    func onReservationCheckInSuccess(message: String)
    func onReservationCheckInFail(message: String)
    /// 取得離店資料 成功
    ///
    /// - Parameter message: 訊息
    func onGetQuerySuccess(message: String)
    /// 取得離店資料 失敗
    ///
    /// - Parameter message: 訊息
    func onGetQueryFail(message: String)
}

let CHECKINLOCATION = "CHECKINLOCATION"

class RegisterCreateViewModel: NSObject,ViewModel {
    
    var delegate: RegisterCreateViewModelDelegate? {
        didSet {
            locationManager = CLLocationManager()
            if delegate == nil {
                self.locationManager.stopUpdatingLocation()
            }else {
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestAlwaysAuthorization()
                locationManager.delegate = self
                locationManager.requestLocation()
            
                if CLLocationManager.locationServicesEnabled() {
                    locationManager.startUpdatingLocation()
                }
            }
        }
    }
    
    //MARK: - 需要的Model
    private let userModel = UserModel.shared!
    
    private let drModelList = DRModelList.shared!
    
    private let reasonModelList = ReasonModelList.shared!
    
    //MARK: -
    /// 定位管理
    private var locationManager: CLLocationManager!
    
    /// 目前位置
    private var nowLocation:CLLocationCoordinate2D?
    
    /// 離店原因清單
    var list:[ReasonModel] {
        return reasonModelList.list
    }
    
    /// 登記名稱
    var accountName: String {
        if userModel.dealertitle == "" {
            return userModel.cname
        }else {
            return "\(userModel.dealertitle)_\(userModel.cname)"
        }
    }
    
    /// 員工代碼
    var empId: String {
        return userModel.empId
    }
    
    /// 離店時間
    var drTime: Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss.S"
        return formatter.date(from: DRModel.shared!.drtime) ?? Date()
    }
    
    /// 離店原因 index
    var reasonIndex: Int {
        var tag = 0
        for i in 0..<reasonModelList.list.count {
            if reasonModelList.list[i].reason_id == DRModel.shared!.reason {
                tag = i
            }
        }
        return tag
    }
    
    /// 離店說明
    var reasonDesc: String {
        return DRModel.shared!.descript
    }
    
    private let defaultRegisterAlarmTime:TimeInterval = -15 * 60 //預設提醒前15分鐘
    
    private let defaultCheckInAlarmTime:TimeInterval = 30 * 60 //預設提醒後30分鐘
    
    private(set) var isCreate:Bool = true
    
    override init() {
        super.init()
    }
    
    //MARK: - custom func
    /// 設定我要出門了的UI顯示
    ///
    /// - Parameters:
    ///   - row: PickerView row
    ///   - completion: (背景顏色,是否可點擊)
    func setGoOutButton(row:Int,completion: @escaping (_ color:UIColor,_ isGoEnabled:Bool) -> Void) {
        if self.list[row].approval_flag && DRModel.shared!.status != .registerComplete {
            completion(UIColor.lightGray,false)
        }else {
            completion(DEFAULT_UI_COLOR,true)
        }
    }
    
    /// 設定預約離店的UI顯示
    ///
    /// - Parameters:
    ///   - row: PickerView row
    ///   - completion: (背景顏色,是否可點擊)
    func setAddButton(row:Int,completion: @escaping (_ color:UIColor,_ isGoEnabled:Bool) -> Void) {
        if DRModel.shared!.status == .registerComplete {
            completion(UIColor.lightGray,false)
        }else {
            completion(DEFAULT_UI_COLOR,true)
        }
    }
    
    //MARK: - api
    //MARK: - 取得離店資料
    /// 取得離店資料
    ///
    /// - Parameter drid: 離店紀錄編號
    func getQuery(drid:String?) {
        isCreate = true
        DRModel.shared!.cleanData()
        if drid != nil {
            isCreate = false
            drModelList.get(drid: drid!, empyId: userModel.empId, showProgress: true, completion: { (result, statusCode, message) in
                if statusCode == .success {
                    self.delegate?.onGetQuerySuccess(message: "onGetQuerySuccess")
                }else {
                    self.delegate?.onGetQueryFail(message: message)
                }
                self.delegate?.reloadView()
            }) { (error) in
                self.delegate?.onGetQueryFail(message: error.localizedDescription)
                self.delegate?.reloadView()
            }
        }
    }
    
    //MARK: - 預約離店
    /// 預約離店
    ///
    /// - Parameters:
    ///   - date: 登記日期
    ///   - reason: 離店原因
    ///   - description: 描述
    func actionReservation(date:String,reasonTag:Int,descript:String) {
        if DRModel.shared!.drid == "" { //判斷是否為新增離店
            let status:DRStatus = (self.list[reasonTag].approval_flag ? DRStatus.registerNotYet : DRStatus.register)
            self.callCreateApi(status: status, date: date, reasonTag: reasonTag, descript: descript, lat: nil, lnt: nil)
        }else {
            self.callUpdateApi(date: date, status: DRModel.shared!.status, reasonTag: reasonTag, descript: descript, lat: self.nowLocation?.latitude, lnt: self.nowLocation?.longitude)
        }
    }
    
    //MARK: - 我出門了
    /// 我出門了
    ///
    /// - Parameters:
    ///   - date: 登記日期
    ///   - reason: 離店原因
    ///   - description: 描述
    func actionCheckIn(date: String, reasonTag: Int, descript: String) {
        locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
        HudUtility.showProgress(title: nil, message: nil)
        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
            if DRModel.shared!.drid == "" { //判斷是否為新增離店
                self.callCreateApi(status: DRStatus.checkIn, date: date, reasonTag: reasonTag, descript: descript, lat: self.nowLocation?.latitude, lnt: self.nowLocation?.longitude)
            }else {
                self.callUpdateApi(date: date, reasonTag: reasonTag, descript: descript, lat: self.nowLocation?.latitude, lnt: self.nowLocation?.longitude)
            }
        }
    }
    
    //MARK: - 新增離店api
    /// 新增離店
    ///
    /// - Parameters:
    ///   - status: 離店狀態
    ///   - date: 日期
    ///   - reasonTag: 離店原因tag
    ///   - descript: 離店描述
    ///   - lat: 緯度
    ///   - lnt: 經度
    private func callCreateApi(status:DRStatus,date:String,reasonTag:Int,descript:String,lat:Double?,lnt:Double?) {
        let loginDic = UserDefaults.standard.dictionary(forKey: AUTOLOGINDIC)!
        let dic = [
            "ad_account" : loginDic.stringValue(ofKey: "ad_account"),
            "empId" : userModel.empId,
            "cname" : userModel.cname,
            "dealercode" : userModel.dealercode,
            "dealertitle" : userModel.dealertitle,
            "drtime" : date,
            "reason" : list[reasonTag].reason_id,
            "description" : descript,
            "status" : status.hashValue,
            "geolocation" : [
                "latitude": lat != nil ? "\(lat!)" : "",
                "longitude": lnt != nil ? "\(lnt!)" : ""
            ]
        ] as [String : Any]
        
        let urlString = "\(SERVER_HOST)/add"
        
        print("TSTAR_API actionReservation \n\(dic.description)")
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: true, completion: { (result, statusCode, message) in
            if statusCode == .success {
                self.setEventStore(status: status, drid: result.stringValue(ofKey: "drid"), date: date, reasonTag: reasonTag, descript: descript)
                self.locationManager.stopUpdatingLocation()
                self.delegate?.onReservationCheckInSuccess(message: "預約成功")
            }else {
                self.delegate?.onReservationCheckInFail(message: message)
            }
        }) { (error) in
            self.delegate?.onReservationCheckInFail(message: error.localizedDescription)
        }
    }
    
    //MARK: - 更新離店
    /// 更新離店
    ///
    /// - Parameters:
    ///   - date: 日期
    ///   - reasonTag: 離店原因Tag
    ///   - descript: 離店描述
    ///   - lat: 緯度
    ///   - lnt: 經度
    private func callUpdateApi(date: String,status:DRStatus = DRStatus.checkIn,reasonTag: Int,descript: String,lat:Double?,lnt:Double?) {
        
        let dic = [
            "drid" : DRModel.shared!.drid,
            "empyId": userModel.empId,
            "drtime": date,
            "reason" : list[reasonTag].reason_id,
            "description": descript,
            "status": status.hashValue,
            "imageId" : "",
            "geolocation" : [
                "latitude": lat != nil ? "\(lat!)" : "",
                "longitude": lnt != nil ? "\(lnt!)" : ""
            ]
        ] as [String : Any]
        
        let urlString = "\(SERVER_HOST)/update"
        
        print("TSTAR_API callUpdateApi \n\(dic.description)")
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: true, completion: { (result, statusCode, message) in
            if statusCode == .success {
                self.locationManager.stopUpdatingLocation()
                self.setEventStore(status: status, drid: result.stringValue(ofKey: "drid"), date: date, reasonTag: reasonTag, descript: descript)
                self.delegate?.onCheckInSuccess(message: "更新成功")
            }else {
                self.delegate?.onCheckInFail(message: message)
            }
        }) { (error) in
            self.delegate?.onCheckInFail(message: error.localizedDescription)
        }
    }
    
    private func updateEventStore(drid:String,date: String) {
        
    }
    
    //MARK: - 設定行事曆提醒
    /// 設定行事曆提醒
    private func setEventStore(status:DRStatus,drid:String,date: String,reasonTag: Int,descript: String) {
        
        let eventStore = EKEventStore()
        eventStore.requestAccess(to: .event, completion: {(granted, error) in //行事曆授權要求
            DispatchQueue.main.async {
                if granted { //假如允許
                    let calendar = eventStore.defaultCalendarForNewEvents
                    let event = EKEvent(eventStore: eventStore)
                    var alarmTime:TimeInterval = 0
                    event.calendar = calendar
                    event.notes = "編號：\(drid)"
                    
                    var isNeedCreate:Bool = false
                    switch status {
                    case .register,.registerNotYet,.registerComplete:
                        if self.list[reasonTag].approval_flag { //假如外訪審核
                            event.title = "【我要外出通知】\n您已登記 \(date) 外訪，離店前請至我要外出APP按壓「我要出門了」。"
                            alarmTime = self.defaultRegisterAlarmTime
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = DATEFORMATTER
                            if let createDate = dateFormatter.date(from: date) {
                                
                                let alarmDate = createDate.addingTimeInterval(alarmTime)
                                let resultTime = alarmDate.timeIntervalSinceReferenceDate - Date().timeIntervalSinceReferenceDate
                                if resultTime / 60.0 < 15 {
                                    return
                                }
                            }
                            
                            isNeedCreate = true
                        }
                    case .checkIn:
                        if self.list[reasonTag].image_flag {
                            alarmTime = self.defaultCheckInAlarmTime
                            if self.list[reasonTag].approval_flag { //假如外訪審核
                                event.title = "【我要外出通知】\n請記得拍照並上傳外訪地點的照片"
                                isNeedCreate = true
                            }else if self.list[reasonTag].reason_name.contains("DM") {
                                event.title = "【我要外出通知】\n請記得拍照並上傳發DM地點/測速畫面&地點照片"
                                isNeedCreate = true
                            }
                        }
                    default:
                        break
                    }
                    
                    if isNeedCreate {
                        
                        let queue = DispatchQueue(label: "com.drapp.eventStore")
                        let calendars = eventStore.calendars(for: .event)
                        queue.sync {
                            for calendar in calendars {
                                // end date (about) one year from now
                                let endDate = Date(timeIntervalSinceNow: 60*60*24*365) //頂多刪除1年內的離店

                                let predicate = eventStore.predicateForEvents(withStart: Date(), end: endDate, calendars: [calendar])

                                let events = eventStore.events(matching: predicate)

                                for event in events {
                                    if event.notes != nil && event.notes!.contains("編號：\(drid)") {
                                        do {
                                            try eventStore.remove(event, span: EKSpan.thisEvent)
                                            break
                                        } catch let e as NSError {
                                            print(e)
                                        }
                                    }
                                }
                            }
                        }
                        
                        queue.sync {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = DATEFORMATTER
                            let date = dateFormatter.date(from: date) ?? Date()
                            let alarm = EKAlarm()
                        
                            alarm.relativeOffset = alarmTime
                            event.addAlarm(alarm)
                            event.startDate = date.addingTimeInterval(alarmTime)
                            event.endDate = date.addingTimeInterval(alarmTime)
                        
                            do {
                                try eventStore.save(event, span: .thisEvent)
                            } catch let e as NSError {
                                print(e)
                                return
                            }
                        }
                    }
                }
            }
        })
    }
}

// MARK: - CLLocationManagerDelegate
extension RegisterCreateViewModel:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        nowLocation = locValue
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
//        let alertController = UIAlertController(title: "", message: "定位功能請允許開啟,才能使用 我要出門了", preferredStyle: .alert)
//        let cancel = UIAlertAction(title: "確認", style: .default) { (action) in
//            alertController.dismiss(animated: true, completion: nil)
//        }
//        let setting = UIAlertAction(title: "設定", style: .destructive, handler: { (action) in
//            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
//        })
//        alertController.addActions(actions: [setting,cancel])
//        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
    }
}

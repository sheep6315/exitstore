//
//  BackStoreViewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/10.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit
import CoreLocation

protocol BackStoreViewModelDelegate: ViewModelDelegate {
    func onUploadPhotoSuccess(message: String,images:[UIImage])
    func onUploadPhotoFail(message: String)
    /// 取得離店資料 成功
    ///
    /// - Parameter message: 訊息
    func onGetQuerySuccess(message: String)
    /// 取得離店資料 失敗
    ///
    /// - Parameter message: 訊息
    func onGetQueryFail(message: String)
    /// 我回店了 成功
    ///
    /// - Parameter message: 訊息
    func onBackStoreSuccess(message: String)
    /// 我回店了 失敗
    ///
    /// - Parameter message: 訊息
    func onBackStoreFail(message: String)
}

class BackStoreViewModel: NSObject,ViewModel {
    
    var delegate: BackStoreViewModelDelegate? {
        didSet {
            self.delegate?.reloadView()
            
            locationManager = CLLocationManager()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.distanceFilter = 100.0
            locationManager.delegate = self
            if CLLocationManager.locationServicesEnabled() {
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    //MARK: - 需要的 Model
    private let userModel = UserModel.shared!
    
    private let drModelList = DRModelList.shared!

    private let reasonModelList = ReasonModelList.shared!
    
    //MARK: - 其餘參數
    private var locationManager: CLLocationManager!
    
    private var nowLocation:CLLocationCoordinate2D?
    
    var reasonList:[ReasonModel] {
        return ReasonModelList.shared!.list
    }
    
    var accountName: String {
        return "\(userModel.dealertitle)_\(userModel.cname)"
    }
    
    var id: String {
        return userModel.empId
    }
    
    var drTime: Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss.S"
        return formatter.date(from: DRModel.shared!.drtime) ?? Date()
    }
    
    /// 離店原因 index
    var reasonIndex: Int {
        var tag = 0
        for i in 0..<reasonModelList.list.count {
            if reasonModelList.list[i].reason_id == DRModel.shared!.reason {
                tag = i
            }
        }
        return tag
    }
    
    /// 離店說明
    var reasonDesc: String {
        return DRModel.shared!.descript
    }
    
    /// 相片展示狀態
    ///
    /// - No: 不需要
    /// - Photo:
    /// - All:
    enum PhotoDisplay {
        case No,Photo,All
    }
    
    private var imageBase64List:[String] = []
    
    private var imagesId:String = ""
    
    override init() {
        super.init()
        
    }
    
    //MARK: - 取得離店資料
    /// 取得離店資料
    ///
    /// - Parameter drid: 離店紀錄編號
    func getQuery(drid:String) {
        DRModel.shared!.cleanData()
        drModelList.get(drid: drid, empyId: userModel.empId, showProgress: true, completion: { (result, statusCode, message) in
            if statusCode == .success {
                self.delegate?.onGetQuerySuccess(message: "onGetQuerySuccess")
            }else {
                self.delegate?.onGetQueryFail(message: message)
            }
            self.delegate?.reloadView()
        }) { (error) in
            self.delegate?.onGetQueryFail(message: error.localizedDescription)
            self.delegate?.reloadView()
        }
    }
    
    //MARK: - 我回店了
    /// 我回店了
    ///
    /// - Parameters:
    ///   - date: 日期
    ///   - reasonTag: 離店原因Tag
    ///   - descript: 離店描述
    ///   - lat: 緯度
    ///   - lnt: 經度
    func actionBackStore() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATEFORMATTER

        let dic = [
            "drid" : DRModel.shared!.drid,
            "empyId": userModel.empId,
            "drtime" : dateFormatter.string(from: Date()),
            "reason" : DRModel.shared!.reason,
            "description": DRModel.shared!.descript,
            "status": DRStatus.backStore.hashValue,
            "imageId" : imagesId,
            "geolocation" : [
                "latitude": nowLocation != nil ? "\(nowLocation!.latitude)" : "",
                "longitude": nowLocation != nil ? "\(nowLocation!.longitude)" : ""
            ]
        ] as [String : Any]
        
        let urlString = "\(SERVER_HOST)/update"
        
        print("BackStoreViewModel actionBackStore \n\(dic.description)")
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: true, completion: { (result, statusCode, message) in
            if statusCode == .success {
                self.delegate?.onBackStoreSuccess(message: "更新成功")
            }else {
                self.delegate?.onBackStoreFail(message: message)
            }
        }) { (error) in
            self.delegate?.onBackStoreFail(message: error.localizedDescription)
        }
    }
    
    //MARK: - 上傳照片
    /// 上傳照片
    ///
    /// - Parameters:
    ///   - drid: drid
    ///   - images: 選取的照片
    func uploadPhoto(drid:String,images:[UIImage]) {
        HudUtility.showProgress(title: nil, message: nil)
        var imagesDic:[[String:String]] = []
        var compressionQuality:CGFloat = 1
        let queue = DispatchQueue(label: "\(Bundle.main.bundleIdentifier ?? "").uploadPhoto")

        images.forEach { (image) in
            queue.sync {
                var imageData = UIImageJPEGRepresentation(image, compressionQuality)
                while compressionQuality > 0 && Double(imageData!.count) / 1024.0 > 500.0 { //壓縮到1MB以內
                    compressionQuality-=0.1
                    imageData = UIImageJPEGRepresentation(image, compressionQuality)
                }
                let picFileBinary = imageData!.base64EncodedString()
                let dic:[String:String] = [
                    "image" : picFileBinary,
                    "latitude" : self.nowLocation != nil ? "\(self.nowLocation!.latitude)" : "",
                    "longitude" : self.nowLocation != nil ? "\(self.nowLocation!.longitude)" : ""
                ]
                imagesDic.append(dic)
            }
        }

        let urlString = "\(SERVER_HOST)/uploadpic"

        let dic:[String:Any] = [
            "drid" : DRModel.shared!.drid,
            "empyId" : self.userModel.empId,
            "dealerCode" : self.userModel.dealercode,
            "images" : imagesDic
        ]
        
        print("BackStoreViewModel uploadPhoto \n\(dic.description)")
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: true, completion: { (result, statusCode, message) in
            if statusCode == .success {
                self.imagesId = result.stringValue(ofKey: "imageId")
                self.delegate?.onUploadPhotoSuccess(message: message, images: images)
            }else {
                self.delegate?.onUploadPhotoFail(message: message)
            }
        }) { (error) in
            self.delegate?.onUploadPhotoFail(message: error.localizedDescription)
        }
    }
    
    func setDisplayPhoto(reasonTag:Int) -> PhotoDisplay {
        if reasonList[reasonTag].image_flag {
            if reasonList[reasonTag].approval_flag {
                return .Photo
            }
            return .All
        }
        return .No
    }
}

extension BackStoreViewModel:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        nowLocation = locValue
    }
}

//
//  TrackViewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/16.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

protocol TrackViewModelDelegate: ViewModelDelegate {
    func onGetTrackListSuccess()
    /// 取得列表 失敗
    ///
    /// - Parameter message: 訊息
    func onGetTrackListFail(message:String)
}

class TrackViewModel: NSObject,ViewModel {
    
    var delegate: TrackViewModelDelegate?
    
    private var trackModelList:TrackModelList = TrackModelList.shared!
    
    private let userModel = UserModel.shared!
    
    private var count = 0
    
    private var row:Int?
    
    private var row2:Int?
    
    /// 即時追蹤列表
    var list:[TrackModelList.Data] {
        if row != nil { //假如點進第一層
            if trackModelList.list[row!].list.count > 1 { //假如第一層只有一筆資料
                if row2 != nil { //假如點進第二層
                    return trackModelList.list[row!].list[row2!].list
                }
                return trackModelList.list[row!].list
            }else if trackModelList.list[row!].list.count == 1 {
                return trackModelList.list[row!].list.first!.list
            }
            return []
        }
        return trackModelList.list
    }
    
    /// 員工人數
    var empyCount:Int {
        var count = 0
        self.list.forEach { (data) in
            count+=(Int(data.model.empycount) ?? 0)
        }
        return count
    }
    
    /// 在店人數
    var inStoreCount:Int {
        var count = 0
        self.list.forEach { (data) in
            count+=((Int(data.model.empycount) ?? 0) - (Int(data.model.drcount) ?? 0))
        }
        return count
    }
    
    /// 休假人數
    var offCount:Int {
        var count = 0
        self.list.forEach { (data) in
            count+=(Int(data.model.offcount) ?? 0)
        }
        return count
    }
    
    /// 出門人數
    var drCount:Int {
        var count = 0
        self.list.forEach { (data) in
            count+=(Int(data.model.drcount) ?? 0)
        }
        return count
    }
    
    /// 警示異常人數
    var alarmcount:Int {
        var count = 0
        self.list.forEach { (data) in
            count+=(Int(data.model.alarmcount) ?? 0)
        }
        return count
    }
    
    override init() {
        super.init()
        
    }
    
    
    /// 取得即時追蹤清單
    func getList() {
        count = 0
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATEFORMATTER
        
        trackModelList.getList(empyId: userModel.empId,query_date: dateFormatter.string(from: Date()),showProgress: true, completion: { (result, statusCode, message) in
            self.delegate?.reloadView()
            if statusCode == .success {
                self.delegate?.onGetTrackListSuccess()
            }else {
                self.delegate?.onGetTrackListFail(message: message)
            }
        }) { (error) in
            self.delegate?.reloadView()
            self.delegate?.onGetTrackListFail(message: error.localizedDescription)
        }
    }
    
    func setListData(row:Int?) {
        if count == 0 {
            self.row = row
        }else if count == 1 {
            self.row2 = row
        }
        self.delegate?.reloadView()
        count+=1
    }
}


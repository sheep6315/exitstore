//
//  LandingViewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/10.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

protocol LandingViewModelDelegate: ViewModelDelegate {
    func onGetServiceListSuccess()
    func onGetServiceListFail()
}

class LandingViewModel: NSObject,ViewModel {
    
    var delegate: LandingViewModelDelegate?
    
    private var serviceModelList:ServiceModelList = ServiceModelList.shared!
    
    private let userModel = UserModel.shared!
    
    /// 服務列表
    var serviceList:[ServiceModel] {
        let character = UserCharacter(rawValue: userModel.titlename)
        if character != nil {
            switch character! {
            case .businessRepresentative:
                return serviceModelList.list.filter { (service) -> Bool in
                    if service.title == "離店登記" || service.title == "離店紀錄" {
                        return true
                    }
                    return false
                }
            case .assistantManager:
                return serviceModelList.list.filter { (service) -> Bool in
                    if service.title == "離店登記" || service.title == "離店紀錄" {
                        return true
                    }
                    return false
                }
            case .manager:
                return serviceModelList.list.filter { (service) -> Bool in
                    if service.title == "離店登記" || service.title == "離店紀錄" || service.title == "外訪件審核" {
                        return true
                    }
                    return false
                }
            case .supervision:
                return serviceModelList.list.filter { (service) -> Bool in
                    if service.title == "外訪件審核" || service.title == "即時追蹤" {
                        return true
                    }
                    return false
                }
            case .headquarters:
                return serviceModelList.list.filter { (service) -> Bool in
                    if service.title == "即時追蹤" {
                        return true
                    }
                    return false
                }
            }
        }
        return []
    }
    
    override init() {
        super.init()
        
    }
    
    /// 取得服務清單
    func getServiceList() {
        serviceModelList.getServiceList(showProgress: true, completion: { (result, statusCode, message) in
            if statusCode == .success {
                self.delegate?.onGetServiceListSuccess()
            }else {
                self.delegate?.onGetServiceListFail()
            }
        }) { (error) in
            self.delegate?.onGetServiceListFail()
        }
    }
}


//
//  ServiceModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/10.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

final class ServiceModelList:NSObject,ApiProtocol {
    
    static var shared: ServiceModelList? = ServiceModelList()
    
    private(set) var list: [ServiceModel] = []
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        list = []
        
        if let resultData = dic[ci:"result"] as? Dictionary<String, Any> {
            if let dataList = resultData[ci: "ADcontext"] as? Array<Dictionary<String, Any>> {
                for data in dataList {
                    let admodel = ServiceModel()
                    admodel.setAttributes(data)
                    list.append(admodel)
                }
            }
        }
    }
    
    //MARK: - 取得服務列表
    func getServiceList(showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic:Dictionary<String,Any> = [
            "statusCode":"00000",
            "result":[
                "ADcontext":[
                    [
                        "title":"離店登記",
                        "image":"Register",
                        "unread":"0"],
                    [
                        "title":"離店紀錄",
                        "image":"Record",
                        "unread":"0"],
                    [
                        "title":"外訪件審核",
                        "image":"Review",
                        "unread":"0"],
                    [
                        "title":"即時追蹤",
                        "image":"Track",
                        "unread":"0"]
                ]
            ]
        ]
        
        ServiceModelList.shared!.setAttributes(dic)
        completion(dic,StatusCode.success,"")
    }
    
    /// 清除資料
    func cleanData() {
        list = []
    }
}

final class ServiceModel:NSObject,ApiProtocol {
    
    static var shared: ServiceModel? = ServiceModel()
    
    var title:String = ""
    
    var image:String = ""
    
    var unread:Int = 0
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        title = dic.stringValue(ofKey: "title")
        image = dic.stringValue(ofKey: "image")
        unread = Int(dic.stringValue(ofKey: "unread")) ?? 0
    }
    
    func updateUnreadCount(showProgress:Bool) {
        
    }
}

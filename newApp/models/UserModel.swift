//
//  UserModel.swift
//  framework
//
//  Created by 詮通電腦-江宗澤 on 2018/3/14.
//  Copyright © 2018年 詮通電腦－劉文景. All rights reserved.
//

import Foundation

/// 使用者Model
final class UserModel:NSObject,ApiProtocol {
    
    static var shared: UserModel? = UserModel()
    
    /// 中文姓名
    private(set) var cname:String = ""
    
    /// 回傳資料時間
    private(set) var createTime:String = ""
    
    /// 店點代碼
    private(set) var dealercode:String = ""
    
    /// 店點名稱
    private(set) var dealertitle:String = ""
    
    /// 部門名稱
    private(set) var deptName:String = ""
    
    /// 部門代碼
    private(set) var deptNo:String = ""
    
    /// 員工代碼
    private(set) var empId:String = ""
    
    /// 電子郵件
    private(set) var mail:String = ""
    
    /// 手機號碼
    private(set) var mobile:String = ""
    
    /// 分機號碼
    private(set) var officeExt:String = ""
    
    /// 員工職稱
    private(set) var titlename:String = ""
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        if let user = dic["user"] as? Dictionary<String, Any> {
            cname = user.stringValue(ofKey: "cname")
            createTime = user.stringValue(ofKey: "createTime")
            dealercode = user.stringValue(ofKey: "dealercode")
            dealertitle = user.stringValue(ofKey: "dealertitle")
            deptName = user.stringValue(ofKey: "deptName")
            deptNo = user.stringValue(ofKey: "deptNo")
            empId = user.stringValue(ofKey: "empId")
            mail = user.stringValue(ofKey: "mail")
            mobile = user.stringValue(ofKey: "mobile")
            officeExt = user.stringValue(ofKey: "officeExt")
            titlename = user.stringValue(ofKey: "titlename")
        }
    }
    
    /// 登入
    ///
    /// - Parameters:
    ///   - msisdn: 手機門號
    ///   - password: 密碼
    ///   - completion: 完成時執行動作
    ///   - failure: 失敗時執行動作
    func login(msisdn:String,password:String,completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void,failure: @escaping (Error) -> Void) {
        TSTAR_API.sharedInstance.login(withMSISDN: msisdn, password: password, showProgress: true, completion: completion, failure: failure)
    }
    
    /// 清除資料
    func cleanData() {
        cname = ""
        createTime = ""
        dealercode = ""
        dealertitle = ""
        deptName = ""
        deptNo = ""
        empId = ""
        mail = ""
        mobile = ""
        officeExt = ""
        titlename = ""
    }
}

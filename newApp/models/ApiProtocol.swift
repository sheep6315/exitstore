//
//  ApiProtocol.swift
//  framework
//
//  Created by 詮通電腦－劉文景 on 2018/3/2.
//  Copyright © 2018年 詮通電腦－劉文景. All rights reserved.
//

/// API協定
protocol ApiProtocol {
    
    /// Singleton
    static var shared: Self? { get }
    
    /// 設定物件屬性值
    ///
    /// - Parameter dic: 物件屬性值
    func setAttributes(_ dic: Dictionary<String, Any>)
}

// MARK: - API共用func
extension ApiProtocol {
    
    /// 取得單筆資料
    ///
    /// - Returns: Self
    func find() -> Self {
        // MARK: - TODO 實作呼叫物件資料API
        self.setAttributes([:])
        return self
    }
    
    /// 取得資料清單
    ///
    /// - Returns: [Self]
    static func findAll() -> [Self] {
        // MARK: - TODO 實作呼叫資料清單API
        return []
    }
    
    /// 資料新增
    func insert() {
        // MARK: - TODO 實作呼叫資料新增API
    }
    
    /// 資料更新
    func update() {
        // MARK: - TODO 實作呼叫資料更新API
    }
    
    /// 資料刪除
    func delete() {
        // MARK: - TODO 實作呼叫資料新增API
    }
}


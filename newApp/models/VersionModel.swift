//
//  VersionModel.swift
//  framework
//
//  Created by 詮通電腦-江宗澤 on 2018/3/14.
//  Copyright © 2018年 詮通電腦－劉文景. All rights reserved.
//

import Foundation

/// 版本Model
final class VersionModel:NSObject,ApiProtocol {
    
    static var shared: VersionModel? = VersionModel()
    
    /// 是否顯示提示
    private(set) var alert:Bool = false
    
    /// 當前版號
    private(set) var curVer:String = ""
    
    /// 當前版本Code
    private(set) var curVerCode:String = ""
    
    /// 提示內容
    private(set) var message:String = ""
    
    /// 最低需求版號
    private(set) var minVer:String = ""
    
    /// 最低需求版本Code
    private(set) var minVerCode:String = ""
    
    /// 提示內容標題
    private(set) var title:String = ""
    
    private(set) var memo1:String = ""
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        guard let versionArray = dic["data"] as? Array<Dictionary<String, Any>> else { return }
        
        if let versionDic = versionArray.first {
            alert = versionDic.stringValue(ofKey: "alert") == "1" ? true : false
            curVer = versionDic.stringValue(ofKey: "curVer")
            curVerCode = versionDic.stringValue(ofKey: "curVerCode")
            memo1 = versionDic.stringValue(ofKey: "memo1")
            if versionDic.stringValue(ofKey: "message") == "" {
                message = "請至App Store下載最新版本\nV\(curVer)"
            }else {
                message = versionDic.stringValue(ofKey: "message")
            }
            minVer = versionDic.stringValue(ofKey: "minVer")
            minVerCode = versionDic.stringValue(ofKey: "minVerCode")
            if versionDic.stringValue(ofKey: "title") == "" {
                title = "版本更新"
            }else {
                title = versionDic.stringValue(ofKey: "title")
            }
        }
    }
    
    func getAppVersion(showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode) -> Void,failure: @escaping (Error) -> Void) {
        let urlString = "https://tstappdev.tstartel.com/AMSWEB/restful/appVersionInfo"
        
        let dic = ["request":
            [
                "osType": "0",
                "appKey": "com.tstartel.tstarcs"]
        ]
        
        print("VersionModel getAppVersion\n\(dic.description)")
        
        let dicData = try! JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions(rawValue: 0))
        let jsonString = String.init(data: dicData, encoding: String.Encoding.utf8)!
        let data = jsonString.data(using: String.Encoding.utf8)!

        
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, data: data, showProgress: showProgress, progressMessage: nil, cacheTime: 0, completion: { (result, statusCode, message) in
            
            VersionModel.shared!.setAttributes(result)
            if result.stringValue(ofKey: "status") == "00000" {
                completion(result,StatusCode.success)
            }else {
                completion(result,StatusCode.fail)
            }
        }, failure: failure)
    }
}

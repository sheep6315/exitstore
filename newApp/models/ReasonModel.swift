//
//  ReasonModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/12.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

/// 離店原因列表
final class ReasonModelList:NSObject,ApiProtocol {
    
    static var shared: ReasonModelList? = ReasonModelList()
    
    private(set) var list: [ReasonModel] = []
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        list = []
        
        if let dataList = dic[ci: "reasons"] as? Array<Dictionary<String, Any>> {
            for data in dataList {
                let model = ReasonModel()
                model.setAttributes(data)
                list.append(model)
            }
        }
    }
    
    //MARK: - 取得離店原因列表
    func getReasonList(empyId: String,showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let urlString = "\(SERVER_HOST)/getreason"
        
        let dic:Dictionary<String,Any> = [
            "empyId" : empyId
        ]
        
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: showProgress, completion: { (result, statusCode, message) in
            ReasonModelList.shared!.setAttributes(result)
            completion(result, statusCode, message)
        }, failure: failure)
    }
    
    /// 清除資料
    func cleanData() {
        list = []
    }
}

/// 離店原因Model
final class ReasonModel:NSObject,ApiProtocol {
    
    static var shared: ReasonModel? = ReasonModel()
    
    /// 警示數量
    var alarm_count: String = ""
    
    /// 警示時間
    var alarm_time: String = ""
    
    /// 是否要外訪審核
    var approval_flag: Bool = false
    
    /// 新增時間
    var create_time: String = ""
    
    /// 新增人員
    var create_user: String = ""
    
    /// 是否要照片上傳
    var image_flag: Bool = false
    
    /// 是否刪除
    var is_delete: Bool = false
    
    /// 離店原因編號
    var reason_id: String = ""
    
    /// 離店原因
    var reason_name: String = ""
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        alarm_count = dic.stringValue(ofKey: "alarm_count")
        alarm_time = dic.stringValue(ofKey: "alarm_time")
        approval_flag = dic.stringValue(ofKey: "approval_flag") == "1" ? true : false
        create_time = dic.stringValue(ofKey: "create_time")
        create_user = dic.stringValue(ofKey: "create_user")
        image_flag = dic.stringValue(ofKey: "image_flag") == "1" ? true : false
        is_delete = dic.stringValue(ofKey: "is_delete") == "Y" ? true : false
        reason_id = dic.stringValue(ofKey: "reason_id")
        reason_name = dic.stringValue(ofKey: "reason_name")
    }
}

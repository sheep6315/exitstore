//
//  TrackModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/16.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

final class TrackModelList:NSObject,ApiProtocol {
    
    static var shared: TrackModelList? = TrackModelList()
    
    private var deptL5List: [TrackModel] = []
    
    private var deptL6List: [TrackModel] = []
    
    private var dealersList: [TrackModel] = []
    
    private(set) var list: [Data] = []
    
    struct Data {
        var model:TrackModel = TrackModel()
        var list:[Data] = []
    }
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        list = []
        deptL5List = []
        if let deptL5Dics = dic[ci: "deptL5"] as? Array<Dictionary<String, Any>> { //第一層
            for deptL5Data in deptL5Dics {
                let deptL5Model = TrackModel()
                deptL5Model.setAttributes(deptL5Data)
                self.deptL5List.append(deptL5Model)
                deptL6List = []
                if let deptL6Dics = deptL5Data[ci: "deptL6"] as? Array<Dictionary<String, Any>> { //第二層
                    for deptL6Data in deptL6Dics {
                        let deptL6Model = TrackModel()
                        deptL6Model.setAttributes(deptL6Data)
                        self.deptL6List.append(deptL6Model)
                        dealersList = []
                        if let dealers = deptL6Data[ci: "dealers"] as? Array<Dictionary<String, Any>> { //第三層
                            for data in dealers {
                                let model = TrackModel()
                                model.setAttributes(data)
                                self.dealersList.append(model)
                            }
                        }
                    }
                }
                var data2:[Data] = []
                deptL6List.forEach { (model) in
                    var data3:[Data] = []
                    dealersList.forEach({ (lastModel) in
                        data3.append(Data(model: lastModel, list: []))
                    })
                    data2.append(Data(model: model, list: data3))
                }
                
                self.list.append(Data(model: deptL5Model, list: data2))
            }
        }
        
    }
    
    //MARK: - 取得即時追蹤列表
    func getList(empyId: String,query_date: String,showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {

        let testDic:[String:Any] = [
            "deptL5" : [
                [
                    "deptname" : "北區直營處",
                    "empycount" : "250",
                    "offcount" : "180",
                    "drcount" : "36",
                    "alarmcount" : "2",
                    "deptL6" : [
                        [
                            "deptname" : "台北直營一部",
                            "empycount" : "32",
                            "offcount" : "6",
                            "drcount" : "2",
                            "alarmcount" : "1",
                            "dealers" : [
                                [
                                    "deptname" : "台北萬芳",
                                    "empycount" : "3",
                                    "offcount" : "2",
                                    "drcount" : "1",
                                    "alarmcount" : "0"
                                ],
                                [
                                    "deptname" : "北投光明",
                                    "empycount" : "2",
                                    "offcount" : "1",
                                    "drcount" : "0",
                                    "alarmcount" : "0"
                                ]
                            ]
                        ],
                        [
                            "deptname" : "台北直營二部",
                            "empycount" : "32",
                            "offcount" : "6",
                            "drcount" : "2",
                            "alarmcount" : "1",
                            "dealers" : [
                                [
                                    "deptname" : "台北萬芳",
                                    "empycount" : "3",
                                    "offcount" : "2",
                                    "drcount" : "1",
                                    "alarmcount" : "0"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "deptname" : "中區直營處",
                    "empycount" : "260",
                    "offcount" : "100",
                    "drcount" : "29",
                    "alarmcount" : "3",
                    "deptL6" : [
                        [
                            "deptname" : "中區直營一部",
                            "empycount" : "250",
                            "offcount" : "180",
                            "drcount" : "36",
                            "alarmcount" : "2",
                            "dealers" : [
                                [
                                    "deptname" : "豐原",
                                    "empycount" : "250",
                                    "offcount" : "180",
                                    "drcount" : "36",
                                    "alarmcount" : "2"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "deptname" : "南區直營處",
                    "empycount" : "190",
                    "offcount" : "120",
                    "drcount" : "35",
                    "alarmcount" : "5",
                    "deptL6" : [
                        [
                            "deptname" : "南區直營一部",
                            "empycount" : "250",
                            "offcount" : "180",
                            "drcount" : "36",
                            "alarmcount" : "2",
                            "dealers" : [
                                [
                                    "deptname" : "高雄",
                                    "empycount" : "250",
                                    "offcount" : "180",
                                    "drcount" : "36",
                                    "alarmcount" : "2"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
//        TrackModelList.shared!.setAttributes(testDic)
//        completion(testDic,StatusCode.success,"")
        
        let urlString = "\(SERVER_HOST)/getrtinfo"
        
        let dic:Dictionary<String,Any> = [
            "empyId" : empyId,
            "query_date" : query_date
        ]
        
        print("TrackModel getrtinfo\n\(dic.description)")
        
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: showProgress, completion: { (result, statusCode, message) in
            TrackModelList.shared!.setAttributes(result)
            completion(result, statusCode, message)
        }, failure: failure)
    }
    
    /// 清除資料
    func cleanData() {
        deptL5List = []
        deptL6List = []
        dealersList = []
        list = []
    }
}

final class TrackModel:NSObject,ApiProtocol {
    
    static var shared: TrackModel? = TrackModel()
    
    /// 名稱
    var deptname:String = ""
    
    /// 員工人數
    var empycount:String = ""
    
    /// 休假人數
    var offcount:String = ""
    
    /// 出門人數
    var drcount:String = ""
    
    /// 警示異常人數
    var alarmcount:String = ""
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        if dic.stringValue(ofKey: "deptname") != "" {
            deptname = dic.stringValue(ofKey: "deptname")
        }else {
            deptname = dic.stringValue(ofKey: "dealername")
        }
        
        empycount = dic.stringValue(ofKey: "empycount")
        offcount = dic.stringValue(ofKey: "offcount")
        drcount = dic.stringValue(ofKey: "drcount")
        alarmcount = dic.stringValue(ofKey: "alarmcount")
    }
}

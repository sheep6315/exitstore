//
//  ReviewModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/16.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

final class ReviewModelList:NSObject,ApiProtocol {
    
    static var shared: ReviewModelList? = ReviewModelList()
    
    private(set) var list: [ReviewModel] = []
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        list = []
        
        if let dataList = dic[ci:"drlist"] as? Array<Dictionary<String, Any>> {
            for data in dataList {
                let model = ReviewModel()
                model.setAttributes(data)
                list.append(model)
            }
        }
    }
    
    //MARK: - 取得外訪件列表
    func getList(empyId: String,showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let urlString = "\(SERVER_HOST)/getapproval"
        
        let dic:Dictionary<String,Any> = [
            "empyId" : empyId
        ]
        
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: true, completion: { (result, statusCode, message) in
            ReviewModelList.shared!.setAttributes(result)
            completion(result, statusCode, message)
        }, failure: failure)
    }
    
    /// 清除資料
    func cleanData() {
        list = []
    }
}

final class ReviewModel:NSObject,ApiProtocol {
    
    static var shared: ReviewModel? = ReviewModel()
    
    /// 姓名
    var cname:String = ""
    
    ///
    var dealer:String = ""
    
    ///
    var descript:String = ""
    
    ///
    var drid:String = ""
    
    var drtime: String = ""
    
    var reason: String = ""
    
    var status: String = ""
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        cname = dic.stringValue(ofKey: "cname")
        dealer = dic.stringValue(ofKey: "dealer")
        descript = dic.stringValue(ofKey: "description")
        drid = dic.stringValue(ofKey: "drid")
        drtime = dic.stringValue(ofKey: "drtime")
        reason = dic.stringValue(ofKey: "reason")
        status = dic.stringValue(ofKey: "status")
    }
    
    func approval(empyId:String,apptime:String,approver:String,showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let urlString = "\(SERVER_HOST)/approval"
        
        let dic = [
            "drid" : self.drid,
            "empyId" : empyId,
            "apptime" : apptime,
            "approver" : approver
        ] as! [String:Any]
        
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: showProgress, completion: completion, failure: failure)
    }
}

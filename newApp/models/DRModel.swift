//
//  ReservationModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/17.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

//MARK: - 離店資料狀態
/// 離店資料狀態
///
/// - register: 登記
/// - registerNotYet: 登記待審核
/// - registerComplete: 登記已審核
/// - checkIn: 離店打卡
/// - backStore: 我回店了
enum DRStatus:String {
    case register = "登記"
    case registerNotYet = "登記待審核"
    case registerComplete = "登記已審核"
    case checkIn = "離店打卡"
    case space = ""
    case backStore = "我回店了"
    
    init(fromHashValue: String) {
        var value:DRStatus = .register
        for status in DRStatus.allCases {
            if String(status.hashValue) == fromHashValue {
                value = status
                break
            }
        }
        self = value
    }
}

//MARK: - 離店物件清單
/// 離店物件清單
final class DRModelList:NSObject,ApiProtocol {
    
    static var shared: DRModelList? = DRModelList()
    
    private(set) var list: [DRModel] = []
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        list = []
        
        if let dataList = dic[ci:"drlist"] as? Array<Dictionary<String, Any>> {
            for data in dataList {
                let drmodel = DRModel()
                drmodel.setAttributes(data)
                list.append(drmodel)
            }
        }
    }
    
    //MARK: - 取得離店列表
    func getList(empyId: String,showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let urlString = "\(SERVER_HOST)/list"
        
        let dic:Dictionary<String,Any> = [
            "empyId" : empyId
        ]
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: true, completion: { (result, statusCode, message) in
            DRModelList.shared!.setAttributes(result)
            completion(result, statusCode, message)
        }, failure: failure)
    }
    
    //MARK: - 取得離店紀錄資料
    /// 取得離店紀錄資料
    ///
    /// - Parameters:
    ///   - drid: 離店紀錄編號
    ///   - showProgress: 是否顯示等待視圖
    ///   - completion: 完成時執行動作
    ///   - failure: 失敗時執行動作
    func get(drid: String,empyId: String,showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let urlString = "\(SERVER_HOST)/query"
        
        let dic:Dictionary<String,Any> = [
            "drid" : drid,
            "empyId" : empyId
        ]
        
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: showProgress, completion: { (result, statusCode, message) in
            DRModel.shared!.setAttributes(result)
            DRModel.shared!.drid = drid
            completion(result, statusCode, message)
        }, failure: failure)
    }
    
    /// 清除資料
    func cleanData() {
        list = []
    }
}

//MARK: - 離店物件
/// 離店物件
final class DRModel:NSObject,ApiProtocol {
    
    static var shared: DRModel? = DRModel()
    
    /// 申請人名稱
    var cname:String = ""
    
    /// 門市名稱
    var dealer:String = ""
    
    /// 離店說明
    var descript: String = ""
    
    /// 離店紀錄編號
    var drid:String = ""

    /// 申請時間
    var drtime:String = ""
    
    /// 離店原因
    var reason:String = ""

    /// 狀態
    var status: DRStatus!
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        drid = dic.stringValue(ofKey: "drid")
        dealer = dic.stringValue(ofKey: "dealer")
        cname = dic.stringValue(ofKey: "cname")
        drtime = dic.stringValue(ofKey: "drtime")
        reason = dic.stringValue(ofKey: "reason")
        descript = dic.stringValue(ofKey: "description")
        status = DRStatus(fromHashValue: dic.stringValue(ofKey: "status"))
    }
    
    func delete(showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let urlString = "\(SERVER_HOST)/delete"
        let dic = [
            "drid" : self.drid,
            "empyId" : UserModel.shared!.empId
        ]
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: true, completion: completion, failure: failure)
    }
    
    /// 清除資料
    func cleanData() {
        DRModel.shared = DRModel()
    }
}

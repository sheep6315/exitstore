//
//  RecordModel.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/16.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

/// 座標
struct Geolocation {
    /// 經度
    var latitude: String = ""
    
    /// 緯度
    var longitude: String = ""
}

final class RecordModelList:NSObject,ApiProtocol {
    
    static var shared: RecordModelList? = RecordModelList()
    
    private(set) var list: [RecordModel] = []
    
    private(set) var exitAmount:String = ""
    
    private(set) var alertAmount:String = ""
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        list = []
        
        if let dataList = dic[ci: "dealers"] as? Array<Dictionary<String, Any>> {
            for data in dataList {
                let model = RecordModel()
                model.setAttributes(data)
                list.append(model)
            }
        }
    }
    
    //MARK: - 取得離店紀錄列表
    func getList(empyId: String,query_date: String,showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let urlString = "\(SERVER_HOST)/queryall"
        
        let dic:Dictionary<String,Any> = [
            "empyId": empyId,
            "query_date": query_date
        ]
        
        print("TSTAR_API queryall \n\(dic.description)")
        
        TSTAR_API.sharedInstance.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: showProgress, completion: { (result, statusCode, message) in
            RecordModelList.shared!.setAttributes(result)
            completion(result, statusCode, message)
        }, failure: failure)
    }
    
    /// 清除資料
    func cleanData() {
        list = []
    }
}

final class RecordModel:NSObject,ApiProtocol {
    
    static var shared: RecordModel? = RecordModel()
    
    /// 中文姓名
    var cname:String = ""
    
    /// 店點代碼
    var dealerCode:String = ""
    
    /// 店點名稱
    var dealerTitle:String = ""
    
    /// 離店時間
    var drtime2:String = ""
    
    /// 回店時間
    var drtime3:String = ""
    
    /// 員工代碼
    var empId:String = ""
    
    /// memo
    var memo:String = ""
    
    /// 離店說明
    var reasonDescription:String = ""
    
    /// 離店原因
    var reasonName: String = ""

    enum RecordType:String {
        case none = "N"
        case normal = "normal"
        case alert = "R"
    }
    
    var type:RecordType = .normal
    
    func setAttributes(_ dic: Dictionary<String, Any>) {
        cname = dic.stringValue(ofKey: "cname")
        dealerCode = dic.stringValue(ofKey: "dealercode")
        dealerTitle = dic.stringValue(ofKey: "dealertitle")
        drtime2 = dic.stringValue(ofKey: "drtime2")
        drtime3 = dic.stringValue(ofKey: "drtime3")
        empId = dic.stringValue(ofKey: "empId")
        
        if dic.stringValue(ofKey: "memo").contains(":") {
            let memoStrArray = dic.stringValue(ofKey: "memo").components(separatedBy: ":")
            if memoStrArray.first != nil {
                for typeCase in RecordType.allCases {
                    if typeCase.rawValue == memoStrArray.first! {
                        type = typeCase
                        break
                    }
                }
                if memoStrArray.last != nil {
                    memo = memoStrArray.last!
                }
            }
        }
        
        reasonDescription = dic.stringValue(ofKey: "reason_description")
        reasonName = dic.stringValue(ofKey: "reason_name")
    }
}

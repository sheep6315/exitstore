//
//  TrackViewController.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/16.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

class TrackViewController: BaseViewController {

    @IBOutlet var viewModel: TrackViewModel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var todayFreeLabel: UILabel!
    
    @IBOutlet weak var todayWorkLabel: UILabel!
    
    @IBOutlet weak var todayInStoreLabel: UILabel!
    
    @IBOutlet weak var todayExitStoreLabel: UILabel!
    
    @IBOutlet weak var todayExitStoreAlertLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateLabel.text = dateFormatter.string(from: Date())
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.tableFooterView = UIView()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tableView.delegate = nil
        tableView.dataSource = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.viewModel.delegate = self
        self.view.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            self.view.isUserInteractionEnabled = true
            self.viewModel.getList()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - @IBAction
    /// 點擊店名
    ///
    /// - Parameter sender:
    @objc private func didTapStoreNameButton(sender:UIButton) {
        let data = viewModel.list[sender.tag]
        if data.list.count == 0 {
//            self.performSegue(withIdentifier: "toRecord", sender: "") //暫時先不跳至離店紀錄
        }else {
            viewModel.setListData(row: sender.tag)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "toRecord" {
//            let vc = segue.destination as! RecordViewController
//            vc.keyword = sender as? String
//        }
    }
}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension TrackViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = viewModel.list[indexPath.row].model
        let cell = tableView.dequeueReusableCell(withIdentifier: "track", for: indexPath) as! TrackTableViewCell
        cell.storeNameButton.tag = indexPath.row
        cell.storeNameButton.addTarget(self, action: #selector(didTapStoreNameButton(sender:)), for: .touchUpInside)
        cell.storeNameButton.setTitle("\(data.deptname)", for: .normal)
        cell.todayFreeLabel.text = data.offcount
        cell.todayWorkLabel.text = data.empycount
        cell.todayInStoreLabel.text = "\((Int(data.empycount) ?? 0) - (Int(data.drcount) ?? 0))"
        cell.todayExitStoreLabel.text =  data.drcount
        cell.todayExitStoreAlertLabel.text = data.alarmcount
        return cell
    }
}

// MARK: - TrackViewModelDelegate
extension TrackViewController:TrackViewModelDelegate {
    func onGetTrackListSuccess() {
        
    }
    
    func onGetTrackListFail(message:String) {
        AlertUtility.show(withMessage: message, sender: self) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func reloadView() {
        todayFreeLabel.text = "\(viewModel.offCount)"
        todayWorkLabel.text = "\(viewModel.empyCount)"
        todayInStoreLabel.text = "\(viewModel.inStoreCount)"
        todayExitStoreLabel.text = "\(viewModel.drCount)"
        todayExitStoreAlertLabel.text = "\(viewModel.alarmcount)"
        self.tableView.reloadData()
    }
}

/// 即時追蹤UITableViewCell
class TrackTableViewCell:UITableViewCell {
    
    @IBOutlet weak var storeNameButton: UIButton!
    
    @IBOutlet weak var todayFreeLabel: UILabel!
    
    @IBOutlet weak var todayWorkLabel: UILabel!
    
    @IBOutlet weak var todayInStoreLabel: UILabel!
    
    @IBOutlet weak var todayExitStoreLabel: UILabel!
    
    @IBOutlet weak var todayExitStoreAlertLabel: UILabel!
}

//
//  LandingViewController.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/10.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

class LandingViewController: BaseViewController {

    @IBOutlet var viewModel: LandingViewModel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.hidesBackButton = true
        
        let screenSize = UIScreen.main.bounds.size
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        flowLayout.minimumLineSpacing      = 1
        flowLayout.minimumInteritemSpacing = 1
        flowLayout.itemSize = CGSize(width: screenSize.width / 2 - 30, height: screenSize.width / 2)
        self.collectionView.setCollectionViewLayout(flowLayout, animated: false)
        self.view.setNeedsUpdateConstraints()
        self.view.layoutIfNeeded()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.delegate = self
        viewModel.getServiceList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - @IBAction
    @IBAction func didTapLogoutButton(_ sender: UIButton) {
        UserDefaults.standard.removeObject(forKey: AUTOLOGINDIC)
        let sb = UIStoryboard(name: "Login", bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = sb.instantiateInitialViewController()
    }
}

extension LandingViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.serviceList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuseCollectionCell", for: indexPath) as! LandingCollectionViewCell
        let data = viewModel.serviceList[indexPath.row]
        cell.titleLabel.text = data.title
        cell.imageView.image = UIImage(named: data.image)
        cell.unreadCountLabel.text = "\(data.unread)"
        cell.unreadCountLabel.isHidden = (data.unread == 0)
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = viewModel.serviceList[indexPath.row]
        self.performSegue(withIdentifier: "to\(data.image)", sender: nil)
    }
}

extension LandingViewController:LandingViewModelDelegate {
    func onGetServiceListSuccess() {
        self.collectionView.reloadData()
    }
    
    func onGetServiceListFail() {
        
    }
    
    func reloadView() {
        
    }
}

/// 首頁項目CollectionViewCell
class LandingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var unreadCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

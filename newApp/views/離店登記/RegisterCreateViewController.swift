//
//  RegisterCreateViewController.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/10.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

class RegisterCreateViewController: BaseViewController {

    @IBOutlet var viewModel: RegisterCreateViewModel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var accountIDLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var dateTextField: UITextField!
    
    @IBOutlet weak var dateButton: UIButton!
    
    @IBOutlet weak var reasonTypeTextField: UITextField!
    
    @IBOutlet weak var descriptionTextField: UITextField!
    
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var goOutButton: UIButton!
    
    /// 離店登記編號
    var drid:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .dateAndTime
        datePickerView.locale = Locale(identifier: "zh_TW")
        datePickerView.date = Date()
        datePickerView.minimumDate = Date()
        datePickerView.addTarget(self, action: #selector(self.dateChanged(datePicker:)), for: .valueChanged)
        dateTextField.inputView = datePickerView
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        reasonTypeTextField.inputView = pickerView
        
        nameLabel.text = viewModel.accountName
        accountIDLabel.text = "(\(viewModel.empId))"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.delegate = self
        viewModel.getQuery(drid: drid)
        reasonTypeTextField.isUserInteractionEnabled = viewModel.isCreate //假如是新增離店紀錄，才可以選擇離店原因
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        viewModel.delegate = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - @IBAction
    /// 點擊日期
    ///
    /// - Parameter sender:
    @IBAction func didTapDateButton(_ sender: UIButton) {
        dateTextField.becomeFirstResponder()
    }

    /// 點擊我是要出門了
    ///
    /// - Parameter sender:
    @IBAction func didTapCheckInButton(_ sender: UIButton) {
        if let pickerView = reasonTypeTextField.inputView as? UIPickerView {
            viewModel.actionCheckIn(date: dateTextField.text!, reasonTag: pickerView.selectedRow(inComponent: 0), descript: descriptionTextField.text!)
        }
    }
    
    /// 點擊預約離店
    ///
    /// - Parameter sender:
    @IBAction func didTapReservationButton(_ sender: UIButton) {
        if let pickerView = reasonTypeTextField.inputView as? UIPickerView {
            viewModel.actionReservation(date: dateTextField.text!, reasonTag: pickerView.selectedRow(inComponent: 0), descript: descriptionTextField.text!)
        }else {
            AlertUtility.show(withMessage: "請選擇離店原因", sender: self)
        }
    }
    
    /// 選擇日期
    ///
    /// - Parameter datePicker:
    @objc func dateChanged(datePicker:UIDatePicker) {
        datePicker.minimumDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = DATEFORMATTER
        let dateStr = formatter.string(from: datePicker.date)
        dateTextField.text = dateStr
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }
    
    //MARK: - custom func
    
}

extension RegisterCreateViewController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let pickerView = textField.inputView as? UIPickerView {
            pickerView.delegate?.pickerView!(pickerView, didSelectRow: pickerView.selectedRow(inComponent: 0), inComponent: 0)
        }else if let datePicker = textField.inputView as? UIDatePicker {
            let formatter = DateFormatter()
            formatter.dateFormat = DATEFORMATTER
            dateTextField.text = formatter.string(from: datePicker.date)
        }
    }
}

// MARK: - UIPickerViewDelegate,UIPickerViewDataSource
extension RegisterCreateViewController: UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.list.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.list[row].reason_name
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let data = viewModel.list[row]
        reasonTypeTextField.text = data.reason_name
        viewModel.setGoOutButton(row: row) { (color, isGoEnabled) in
            self.goOutButton.backgroundColor = color
            self.goOutButton.isUserInteractionEnabled = isGoEnabled
        }
        viewModel.setAddButton(row: row) { (color, isGoEnabled) in
            self.addButton.backgroundColor = color
            self.addButton.isUserInteractionEnabled = isGoEnabled
        }
    }
}

// MARK: - RegisterCreateViewModelDelegate
extension RegisterCreateViewController:RegisterCreateViewModelDelegate {
    func onReservationCheckInSuccess(message: String) {
        AlertUtility.show(withMessage: message, sender: self) { (action) in
            self.performSegue(withIdentifier: "backToRegisterVC", sender: nil)
        }
    }
    
    func onReservationCheckInFail(message: String) {
        AlertUtility.show(withMessage: message, sender: self)
    }
    
    func onGetQuerySuccess(message: String) {

    }
    
    func onGetQueryFail(message: String) {
        AlertUtility.show(withMessage: message, sender: self)
    }
    
    func onCheckInFail(message: String) {
        AlertUtility.show(withMessage: message, sender: self)
    }
    
    func onCheckInSuccess(message: String) {
        AlertUtility.show(withMessage: message, sender: self) { (action) in
            self.performSegue(withIdentifier: "backToRegisterVC", sender: nil)
        }
    }

    func reloadView() {
        nameLabel.text = viewModel.accountName
        accountIDLabel.text = "(\(viewModel.empId))"
        if let datePicker = dateTextField.inputView as? UIDatePicker {
            datePicker.date = viewModel.drTime
            dateTextField.inputView = datePicker
            let formatter = DateFormatter()
            formatter.dateFormat = DATEFORMATTER
            dateTextField.text = formatter.string(from: viewModel.drTime)
        }
        if let pickerView = reasonTypeTextField.inputView as? UIPickerView {
            pickerView.selectRow(viewModel.reasonIndex, inComponent: 0, animated: true)
            self.pickerView(pickerView, didSelectRow: viewModel.reasonIndex, inComponent: 0)
        }
        if viewModel.list[viewModel.reasonIndex].approval_flag && !viewModel.isCreate {
            dateButton.isUserInteractionEnabled = false
            descriptionTextField.isUserInteractionEnabled = false
        }else {
            dateButton.isUserInteractionEnabled = true
            descriptionTextField.isUserInteractionEnabled = true
        }
        
        descriptionTextField.text = viewModel.reasonDesc
        
    }
}

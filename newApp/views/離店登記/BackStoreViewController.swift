//
//  BackStoreViewController.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/12.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit
import BSImagePicker
import Photos

class BackStoreViewController: BaseViewController {

    @IBOutlet var viewModel: BackStoreViewModel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var idLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var reasonLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var uploadPhotoStackView: UIStackView!
    
    @IBOutlet weak var takeAlbumButton: UIButton!
    
    @IBOutlet weak var takePhotoButton: UIButton!
    
    @IBOutlet var imageViewList: [UIImageView]!
    
    @IBOutlet weak var editCameraWidget: EditCameraWidget!
    
    @IBOutlet weak var backStoreButton: UIButton!
    
    var drid: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.delegate = self
        viewModel.getQuery(drid: drid)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - @IBAction
    /// 點擊拍照
    ///
    /// - Parameter sender:
    @IBAction func didTapTakePhotoButton(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func didTapBackStoreButton(_ sender: UIButton) {
        viewModel.actionBackStore()
    }
    
    @IBAction func didTapPhotoLibraryButton(_ sender: UIButton) {
        let vc = BSImagePickerViewController()
        var number = 0
        for imageView in self.imageViewList {
            if imageView.image == nil {
                number+=1
            }
        }
        vc.maxNumberOfSelections = number
        bs_presentImagePickerController(vc, animated: true, select: nil, deselect: nil, cancel: nil, finish: { (assets: [PHAsset]) in
            DispatchQueue.global().async {
                var images:[UIImage] = []
                assets.forEach({ (asset) in
                    images.append(asset.uiImage!)
                })
                self.viewModel.uploadPhoto(drid: self.drid, images: images)
            }
        }, completion: nil)
    }
}

// MARK: - BackStoreViewModelDelegate
extension BackStoreViewController:BackStoreViewModelDelegate {
    func onBackStoreSuccess(message: String) {
        AlertUtility.show(withMessage: message, sender: self) { (action) in
            self.performSegue(withIdentifier: "backToRegisterVC", sender: nil)
        }
    }
    
    func onBackStoreFail(message: String) {
        AlertUtility.show(withMessage: message, sender: self)
    }
    
    func onGetQuerySuccess(message: String) {
        
    }
    
    func onGetQueryFail(message: String) {
        
    }
    
    func onUploadPhotoSuccess(message: String,images: [UIImage]) {
        var row = 0
        var hidePhotoStackView:Bool = true
        for imageView in self.imageViewList {
            if imageView.image == nil {
                if row < images.count {
                    imageView.image = images[row]
                    row+=1
                    imageView.isHidden = false
                }
            }
            if imageView.image == nil {
                hidePhotoStackView = false
            }
        }
        self.uploadPhotoStackView.isHidden = hidePhotoStackView
        backStoreButton.backgroundColor = DEFAULT_UI_COLOR
        backStoreButton.isUserInteractionEnabled = true
    }
    
    func onUploadPhotoFail(message: String) {
        AlertUtility.show(withMessage: message, sender: self)
    }
    
    func reloadView() {
        nameLabel.text = viewModel.accountName
        idLabel.text = viewModel.id
        let formatter = DateFormatter()
        formatter.dateFormat = DATEFORMATTER
        dateLabel.text = formatter.string(from: viewModel.drTime)
        reasonLabel.text = viewModel.reasonList[viewModel.reasonIndex].reason_name
        descriptionLabel.text = viewModel.reasonDesc
        if viewModel.reasonList[viewModel.reasonIndex].image_flag {
            backStoreButton.backgroundColor = UIColor.lightGray
            backStoreButton.isUserInteractionEnabled = false
        }else {
            backStoreButton.backgroundColor = DEFAULT_UI_COLOR
            backStoreButton.isUserInteractionEnabled = true
        }
        switch viewModel.setDisplayPhoto(reasonTag: viewModel.reasonIndex) {
        case .No:
            uploadPhotoStackView.isHidden = true
        case .Photo:
            uploadPhotoStackView.isHidden = false
            takeAlbumButton.isHidden = true
        case .All:
            uploadPhotoStackView.isHidden = false
            takeAlbumButton.isHidden = false
            takePhotoButton.isHidden = false
        }
    }
}

// MARK: - UIImagePickerControllerDelegate
extension BackStoreViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var image = UIImage()
        if let originImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            image = originImage
        }else if let editImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            image = editImage
        }
        dismiss(animated: false) {
            self.editCameraWidget.delegate = self
            self.editCameraWidget.initView(photo: image)
        }
    }
}

extension BackStoreViewController:EditCameraDelegate {
    func retake() {
        didTapTakePhotoButton(UIButton())
    }
    
    func upload(image:UIImage) {
        self.viewModel.uploadPhoto(drid: drid, images: [image])
    }
    
    func exitStoreCancel() {
        
    }   
}

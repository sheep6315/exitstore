//
//  RegisterViewController.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/17.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController {
    
    @IBOutlet var viewModel: RegisterViewModel!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var registerAmountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.delegate = self
        viewModel.getDRList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - @IBAction
    @IBAction func didTapCreateButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toCreate", sender: nil)
    }
    
    //MARK: -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEdit" {
            let vc = segue.destination as! RegisterCreateViewController
            vc.drid = sender as? String
        }else if segue.identifier == "toFinish" {
            if let vc = segue.destination as? BackStoreViewController {
                vc.drid = sender as! String
            }
        }
    }
    
    @IBAction func unwindToRegisterVC(segue:UIStoryboardSegue) {
        
    }
}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension RegisterViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = viewModel.list[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "RegisterTableViewCell", for: indexPath) as! RegisterTableViewCell
        cell.nameLabel.text = data.cname
        cell.timeLabel.text = data.drtime
        cell.reasonLabel.text = data.reason
        cell.descriptionLabel.text = data.descript
        cell.agreeLabel.text = data.status.rawValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = viewModel.list[indexPath.row]
        if data.status != .registerNotYet {
            if data.status.hashValue < 3 {
                self.performSegue(withIdentifier: "toEdit", sender: data.drid)
            }else {
                self.performSegue(withIdentifier: "toFinish", sender: data.drid)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return viewModel.setDelete(row: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "刪除"
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            let ok = UIAlertAction(title: "確認", style: .default) { (action) in
                self.viewModel.deleteReservationList(row: indexPath.row)
            }
            AlertUtility.showWithActions(withMessage: "是否刪除?", sender: self, actions: [cancel,ok])
        }
    }
}

// MARK: - RegisterViewModelDelegate
extension RegisterViewController:RegisterViewModelDelegate {
    
    func deleteReservationSuccess(message: String) {
        AlertUtility.show(withMessage: message, sender: self)
    }
    
    func deleteReservationFail(message: String) {
        AlertUtility.show(withMessage: message, sender: self)
    }
    
    func onReservationListFail(message: String) {
        AlertUtility.show(withMessage: message, sender: self)
    }
    
    func reloadView() {
        registerAmountLabel.text = "已登記\(viewModel.list.count)筆"
        self.tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
            self.tableView.setContentOffset(.zero, animated: true)
        }
        
    }
}

class RegisterTableViewCell:UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var reasonLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var agreeLabel: UILabel!
    
}

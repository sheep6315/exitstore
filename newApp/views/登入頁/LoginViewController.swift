//
//  LoginViewController.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet var viewModel: LoginViewModel!
    
    @IBOutlet weak var accountTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!

    @IBOutlet weak var versionLabel: UILabel!
    
    private enum UserType:String {
        case sales = "rickyliu"
        case assistantStoreManager  = "wendychiang"
        case storeManager = "winnie01chen"
        case supervision = "nickhu"
        case headquarters = "michaelchai"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        versionLabel.text = "版本 \(Bundle.main.releaseVersionNumber ?? "")"
        viewModel.delegate = self
//        accountTextField.text = "winnie01chen"
//        passwordTextField.text = "12345678"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let loginDic = UserDefaults.standard.dictionary(forKey: AUTOLOGINDIC) {
            accountTextField.text = loginDic.stringValue(ofKey: "ad_account")
            passwordTextField.text = loginDic.stringValue(ofKey: "password")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// 點擊登入
    @IBAction func didTapLoginButton(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let verify = ValidatorUtility.verify(withSettings: [
            ValidateRule(withLabel: "帳號", value: accountTextField.text!, rules: [.Required]),
            ValidateRule(withLabel: "密碼", value: passwordTextField.text!, rules: [.Required])]
        )
        if verify {
            viewModel.actionLogin(msisdn: accountTextField.text!, password: passwordTextField.text!)
        }else {
            AlertUtility.show(withMessage: ValidatorUtility.errors.first!, sender: self)
        }
    }
    
    /// 選擇角色
    @IBAction func didTapChangeUserButton(_ sender: UIButton) {
        self.view.endEditing(true)
//        for user in UserType.allCases {
//            if user.hashValue == sender.tag {
//                accountTextField.text = user.rawValue
//                break
//            }
//        }
    }
    
    @IBAction func unwindLoginVC(segue: UIStoryboardSegue) {
        
    }
}

// MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textField.returnKeyType == .next {
            passwordTextField.becomeFirstResponder()
        }else if textField.returnKeyType == .go {
            didTapLoginButton(UIButton())
        }
        return true
    }
}

// MARK: - LoginViewModelDelegate
extension LoginViewController:LoginViewModelDelegate {
    func onLoginSuccess() {
        if viewModel.character != nil && viewModel.character! == .headquarters {
            self.performSegue(withIdentifier: "toTrack", sender: nil)
        } else {
            self.performSegue(withIdentifier: "toLanding", sender: nil)
        }
    }
    
    func onLoginFail(message: String) {
        AlertUtility.show(withMessage: message, sender: self)
    }
    
    func onGetAppVersionSuccess() {
        
    }
    
    func onGetAppVersionFail(message: String) {
        
    }
    
    func reloadView() {
        
    }
}

//
//  RecordViewController.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/16.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

class RecordViewController: BaseViewController {

    /// 離店人數
    @IBOutlet weak var exitAmountLabel: UILabel!
    
    /// 離店異常人數
    @IBOutlet weak var alertAmountLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var keywordTextField: UITextField!
    
    @IBOutlet var viewModel: RecordViewModel!
    
    /// 關鍵字
    var keyword: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        viewModel.delegate = self
        keywordTextField.text = keyword
        viewModel.getList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - @IBAction
    /// 點擊搜尋按鈕
    ///
    /// - Parameter sender:
    @IBAction func didTapSearchButton(_ sender: UIButton) {
        self.view.endEditing(true)
        viewModel.getList(query_date: keywordTextField.text!)
    }
}

// MARK: - UITextFieldDelegate
extension RecordViewController:UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textField.returnKeyType == .search {
            didTapSearchButton(UIButton())
        }
        return true
    }
}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension RecordViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = viewModel.list[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecordTableViewCell", for: indexPath) as! RecordTableViewCell
        cell.nameLabel.text = "\(data.dealerTitle)_\(data.cname)"
        cell.dateRangeLabel.text = "\(data.drtime2) ~ \(data.drtime3)"
        cell.reasonLabel.text = data.reasonName
        cell.timeLabel.text = ""
        cell.statusLabel.text = data.memo
        switch data.type {
        case .none:
            cell.backgroundColor = cell.noneColor
            cell.statusLabel.textColor = cell.noneTextColor
        case .normal:
            cell.backgroundColor = cell.normalColor
            cell.statusLabel.textColor = cell.normalTextColor
        case .alert:
            cell.backgroundColor = cell.alertColor
            cell.statusLabel.textColor = cell.alertTextColor
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                    self.tableViewHeight.constant = tableView.contentSize.height
                })
            }
        }
    }
}

// MARK: - RecordViewModelDelegate
extension RecordViewController:RecordViewModelDelegate {
    func onGetRecordListSuccess() {
        self.tableView.isHidden = false
    }
    
    func onGetRecordListFail() {
        self.tableViewHeight.constant = 100
        self.tableView.isHidden = true
    }
    
    func reloadView() {
        exitAmountLabel.text = viewModel.exitAmount
        alertAmountLabel.text = viewModel.alertAmount
        self.tableView.reloadData()
    }
}

/// 離店紀錄UITableViewCell
class RecordTableViewCell:UITableViewCell {
    
    /// 店名
    @IBOutlet weak var nameLabel: UILabel!
    
    /// 時間區間
    @IBOutlet weak var dateRangeLabel: UILabel!
    
    /// 離店原因
    @IBOutlet weak var reasonLabel: UILabel!
    
    /// 離店耗時
    @IBOutlet weak var timeLabel: UILabel!
    
    /// 狀態
    @IBOutlet weak var statusLabel: UILabel!
    
    var alertColor:UIColor = UIColor(red: 1, green: 204/255, blue: 252/255, alpha: 1)
    var alertTextColor:UIColor = UIColor.red
    var noneColor:UIColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    var noneTextColor:UIColor = UIColor.lightGray
    var normalColor:UIColor = UIColor(red: 202/255, green: 236/255, blue: 253/255, alpha: 1)
    var normalTextColor:UIColor = UIColor.red

}

//
//  ReviewViewController.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/18.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

class ReviewViewController: BaseViewController {

    @IBOutlet var viewModel: ReviewViewModel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    /// 外訪件數量
    @IBOutlet weak var amountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        viewModel.delegate = self
        viewModel.getReviewList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - @IBAction
    @objc private func didTapConfirmButton(sender:UIButton) {
        viewModel.actionApproval(row: sender.tag)
        print("didTapConfirmButton")
    }
}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension ReviewViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = viewModel.list[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as! ReviewTableViewCell
        cell.nameLabel.text = data.cname
        cell.dateLabel.text = data.drtime
        cell.reasonLabel.text = viewModel.getReason(id: data.reason)
        cell.descLabel.text = data.descript
        cell.confirmButton.addTarget(self, action: #selector(self.didTapConfirmButton(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                    self.tableViewHeight.constant = tableView.contentSize.height
                })
            }
        }
    }
}

// MARK: - ReviewViewModelDelegate
extension ReviewViewController: ReviewViewModelDelegate {
    func onGetReviewListSuccess() {
        
    }
    
    func onGetReviewListFail(message: String) {
        AlertUtility.show(withMessage: message, sender: self) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func onReviewSuccess(message: String) {
        
    }
    
    func onReviewFail(message: String) {
        
    }
    
    func reloadView() {
        self.tableView.reloadData()
        amountLabel.text = "已登記\(viewModel.list.count)筆外訪件"
    }
}

/// 外訪件TableViewCell
class ReviewTableViewCell: UITableViewCell {
    
    /// 姓名
    @IBOutlet weak var nameLabel: UILabel!
    
    /// 日期
    @IBOutlet weak var dateLabel: UILabel!
    
    /// 離店原因
    @IBOutlet weak var reasonLabel: UILabel!
    
    /// 描述
    @IBOutlet weak var descLabel: UILabel!
    
    /// 審核按鈕
    @IBOutlet weak var confirmButton: UIButton!
    
    var pk: String = ""
    
    /// 是否審核完成
    var isReviewComplete:Bool = false {
        didSet {
            self.setConfirmButton()
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: - custom func
    private func setConfirmButton() {
        if isReviewComplete {
            self.confirmButton.leftImage(image: UIImage(named: "icon_backMenu")!, renderMode: .alwaysOriginal)
            confirmButton.setTitleColor(UIColor(red: 135/255, green: 40/255, blue: 110/255, alpha: 1), for: .normal)
            confirmButton.isUserInteractionEnabled = false
        }else {
            self.confirmButton.leftImage(image: UIImage(named: "icon_menu")!, renderMode: .alwaysOriginal)
            confirmButton.setTitleColor(UIColor.lightGray, for: .normal)
            confirmButton.isUserInteractionEnabled = true
        }
    }
}

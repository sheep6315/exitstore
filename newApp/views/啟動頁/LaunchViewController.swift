//
//  LaunchViewController.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

/// 自定義啟動頁面
class LaunchViewController: UIViewController {
    
    @IBOutlet var viewModel: LaunchViewModel!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        viewModel.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // MARK: TODO - 初始化設定
        viewModel.getAppVersion()
        viewModel.getReasonList()
//        sleep(2)
        // 初始化完成導頁
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            UIApplication.shared.keyWindow!.rootViewController = storyboard.instantiateInitialViewController()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

// MARK: - LaunchViewModelDelegate
extension LaunchViewController: LaunchViewModelDelegate {
    func onGetAppVersionSuccess() {
        if viewModel.alert {
            
            if VERSION_CODE < viewModel.minVerCode {
                UserDefaults.standard.set(VersionStatus.forceUpdate.rawValue, forKey: VERSION_STATUS)
            }
            else if VERSION_CODE < viewModel.curVerCode {
                UserDefaults.standard.set(VersionStatus.remindUpdate.rawValue, forKey: VERSION_STATUS)
            }
            else if VERSION_CODE > viewModel.curVerCode {
                UserDefaults.standard.set(VersionStatus.notReleased.rawValue, forKey: VERSION_STATUS)
            }
            else {
                UserDefaults.standard.set(VersionStatus.noUpdateStatus.rawValue, forKey: VERSION_STATUS)
            }
            
            let alert = UIAlertController(title: viewModel.title, message: nil, preferredStyle: .alert)
            
            let openAppStore = UIAlertAction(title: "確定", style: .default, handler: { (action: UIAlertAction) in
                if let url = self.viewModel.updateUrl {
                    UIApplication.shared.open(url, options: [:], completionHandler: { (_) in
                        exit(0)
                    })
                }
            })
            
            let status = UserDefaults.standard.value(forKey: VERSION_STATUS) as! Int
            
            if status == VersionStatus.forceUpdate.rawValue {
                alert.addAction(openAppStore)
            }
            else if status == VersionStatus.remindUpdate.rawValue {
                let remindMeLater = UIAlertAction(title: "稍候提醒", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction) in
                    
                    DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                        let mainSB = UIStoryboard(name: "Main", bundle: nil)
                        self.present(mainSB.instantiateInitialViewController()!, animated: false, completion: nil)
                    })
                    
                })
                alert.addAction(remindMeLater)
                alert.addAction(openAppStore)
            }
            
            if status < VersionStatus.notReleased.rawValue {
                
                let storyBoard = UIStoryboard(name: "TStarWebView", bundle: nil)
                let webViewVC = storyBoard.instantiateViewController(withIdentifier: "TStarWebViewController") as! TStarWebViewController
                webViewVC.preferredContentSize.height = 300
                alert.setValue(webViewVC, forKey: "contentViewController")
                self.appDelegate.window?.rootViewController?.present(alert, animated: true, completion: {
                    webViewVC.contentWebView.backgroundColor = UIColor.clear
                    webViewVC.contentWebView.scalesPageToFit = false
                    webViewVC.contentWebView.loadHTMLString(self.viewModel.message, baseURL: nil)
                })
                
            }
        }
    }
    
    func onGetAppVersionFail(message: String) {
        
    }
    
    
    func onGetReasonTypeSuccess() {
        
    }
    
    func onGetReasonTypeFail() {
        
    }
    
    
    func reloadView() {
        
    }
}

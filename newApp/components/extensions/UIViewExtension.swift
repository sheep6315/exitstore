//
//  UIViewExtension.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/12.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

extension UIView {
    
    enum ViewSide {
        case Left, Right, Top, Bottom
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth:CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor {
        get {
            return UIColor(cgColor: layer.borderColor ?? UIColor.clear.cgColor)
        }
        set {
            layer.borderColor = newValue.cgColor
        }
    }
    
    /// 增加邊框 Example use: myView.addBorder(toSide: .Left, withColor: UIColor.redColor().CGColor, andThickness: 1.0)
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        
        layer.addSublayer(border)
    }
    
    /// 取得當前輸入元件
    ///
    /// - Returns: 當前輸入元件
    func findResponder() -> (current: UIView?, prev: UIView?, next: UIView?) {
        let inputFields = self.collectionInputFields()
        var current: UIView?
        var prev: UIView?
        var next: UIView?
        for (index, field) in inputFields.enumerated() {
            if field.isFirstResponder {
                if index != 0 {
                    prev = inputFields[index-1]
                }
                if index != inputFields.count-1 {
                    next = inputFields[index+1]
                }
                current = field
                break
            }
        }
        return (current: current, prev: prev, next: next)
    }
    
    /// 取得頁面全部輸入欄位
    ///
    /// - Returns: 輸入欄位清單
    func collectionInputFields() -> [UIView] {
        var fields: [UIView] = []
        for subview in self.subviews {
            if let textField = subview as? UITextField {
                if textField.isEnabled == true && textField.isUserInteractionEnabled == true {
                    fields.append(textField)
                }
            } else if let textView = subview as? UITextView {
                if textView.isHidden == false && textView.isEditable == true {
                    fields.append(textView)
                }
            } else if let tableView = subview as? UITableView {
                for cell in tableView.visibleCells {
                    fields.append(contentsOf: cell.contentView.collectionInputFields())
                }
            } else {
                fields.append(contentsOf: subview.collectionInputFields())
            }
        }
        return fields
    }
    
    func findFirstSubview(_ className:String) -> UIView? {
        for subview in self.subviewsRecursive() {
            if subview.className() == className {
                return subview
            }
        }
        return nil
    }
    
    func subviewsRecursive() -> [UIView] {
        return subviews + subviews.flatMap { $0.subviewsRecursive() }
    }
}

extension NSObject {
    func className() -> String {
        return String(describing: type(of: self))
    }
}

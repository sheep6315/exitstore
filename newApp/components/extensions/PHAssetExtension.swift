//
//  PHAssetExtension.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/13.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit
import Photos

extension PHAsset {
    var uiImage:UIImage? {
        get{
            var img: UIImage?
            let manager = PHImageManager.default()
            let options = PHImageRequestOptions()
            options.version = .original
            options.isSynchronous = true
            manager.requestImageData(for: self, options: options) { data, _, _, _ in
                
                if let data = data {
                    img = UIImage(data: data)
                }
            }
            return img
        }
    }
}

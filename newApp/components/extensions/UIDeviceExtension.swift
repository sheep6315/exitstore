//
//  UIDeviceExtension.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/12.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

// MARK: - UIDevice Extension
extension UIDevice {
    
    /// 是否為iPhone X
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if self.isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6"
    }
    
    /// 是否為模擬器
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}

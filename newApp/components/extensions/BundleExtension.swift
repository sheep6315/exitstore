//
//  BundleExtension.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/12.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

/// The bundle key for the association
private var kBundleKey: UInt8 = 0

// MARK: - Bundle Extension
extension Bundle {
    
    /// 版本號
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    /// 建制版本
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}

//
//  DispatchQueueExtension.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/13.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import Foundation

extension DispatchQueue {
    
    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
}

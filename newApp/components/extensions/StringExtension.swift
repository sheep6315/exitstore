//
//  StringExtension.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//
import Foundation

// MARK: - String extension
extension String {
    
    //使用正则表达式替换
    func pregReplace(pattern: String, with: String,
                     options: NSRegularExpression.Options = []) -> String {
        let regex = try! NSRegularExpression(pattern: pattern, options: options)
        return regex.stringByReplacingMatches(in: self, options: [],
                                              range: NSMakeRange(0, self.count),
                                              withTemplate: with)
    }
    
    /// 判斷中文字
    var containsChineseCharacters: Bool {
        return self.range(of: "\\p{Han}", options: .regularExpression) != nil
    }
    
    /// json解密
    ///
    /// - Returns: Dictionary<String, Any>?
    public func jsonDecode() -> Dictionary<String, Any>? {
        var dic: Dictionary<String, Any> = [:]
        do {
            dic = try JSONSerialization.jsonObject(with: self.data(using: .utf8)!, options: []) as! Dictionary<String, Any>
            return dic
        } catch {
            return nil
        }
    }
    
    /// 刪除前綴
    ///
    /// - Parameter prefix: 字詞
    mutating func deletingPrefix(_ prefix: String) {
        guard self.hasPrefix(prefix) else { return }
        self = String(self.dropFirst(prefix.count))
    }
}


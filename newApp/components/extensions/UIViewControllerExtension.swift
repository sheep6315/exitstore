//
//  UIViewControllerExtension.swift
//  DEV
//
//  Created by 詮通電腦-江宗澤 on 2018/8/10.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

extension UIViewController {
    var appDelegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}

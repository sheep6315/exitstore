//
//  UIAlertControllerExtension.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/12.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

// MARK: - UIAlertController extension
extension UIAlertController {
    
    /// 增加複數按鈕
    ///
    /// - Parameter actions: 按鈕清單
    public func addActions(actions: [UIAlertAction]) {
        for action in actions {
            self.addAction(action)
        }
    }
    
    public func show(viewController: UIViewController) {
        
        viewController.present(self, animated: true, completion: nil)
    }
    
}

//
//  DictionaryExtension.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//
import Foundation

extension Dictionary where Key: ExpressibleByStringLiteral, Value: Any{
    
    subscript(ci key : Key) -> Value? {
        get {
            let searchKey = String(describing: key).lowercased()
            for k in self.keys {
                let lowerK = String(describing: k).lowercased()
                if searchKey == lowerK {
                    return self[k]
                }
            }
            return nil
        }
    }
    
    public func valueIsString(ofKey key: String) -> Bool {
        let dic = (self as Any) as? Dictionary<String, Any>
        return dic?[key] as? String != nil
    }
    
    public func stringValue(ofKey key: String) -> String {
        let dic = (self as Any) as? Dictionary<String, Any>
        var theString = ""
        if let value = dic?[ci: key] as? String {
            theString = value
        }
        else if let value = dic?[ci: key] as? NSNumber {
            theString = value.stringValue
        }
        return theString
    }
    
    func toJsonString() -> String {
        guard let data = try? JSONSerialization.data(withJSONObject: self, options: []) else {
            return ""
        }
        return String(data: data, encoding: String.Encoding.utf8) ?? ""
    }
}

//
//  Widgets.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/16.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//
import UIKit

/// 共用顯示部件
class Widgets: UIView {
    
    /// 顯示內容
    private(set) var contentView:UIView?
    
    /// 當前顯示UIViewController
    private var currentViewController: UIViewController?
    
    /// 導頁目標UIViewController
    private var destinationViewController: UIViewController?
    
    final override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    
    final override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        self.contentView?.prepareForInterfaceBuilder()
    }
    
    /// 設定顯示內容
    final func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        self.contentView = view
        self.currentViewController = UIApplication.shared.keyWindow?.rootViewController
        self.destinationViewController = UIApplication.shared.keyWindow?.rootViewController
        self.setup()
    }
    
    /// 取得模板內容
    ///
    /// - Returns: 模板內容
    final func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    final override func didMoveToWindow() {
        if self.window == nil {
            if self.currentViewController == self.destinationViewController {
                self.didDisappear()
            }
            self.destinationViewController = nil
        } else {
            if self.currentViewController == UIApplication.shared.keyWindow?.rootViewController {
                self.didAppear()
            }
            self.destinationViewController = UIApplication.shared.keyWindow?.rootViewController
        }
    }
    
    final override func willMove(toWindow newWindow: UIWindow?) {
        if newWindow == nil {
            if self.currentViewController == self.destinationViewController {
                self.willDisappear()
            }
        } else {
            if self.currentViewController == UIApplication.shared.keyWindow?.rootViewController {
                self.willAppear()
            }
        }
    }
    
    /// 初始化設定
    func setup() {}
    
    /// Called when the widget is about to made visible. Default does nothing
    func willAppear() {}
    
    /// Called when the widget has been fully transitioned onto the screen. Default does nothing
    func didAppear() {}
    
    /// Called when the widget is dismissed, covered or otherwise hidden. Default does nothing
    func willDisappear() {}
    
    /// Called after the widget was dismissed, covered or otherwise hidden. Default does nothing
    func didDisappear() {}
}


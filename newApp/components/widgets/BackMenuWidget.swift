//
//  BackMenuWidget.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/18.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

/// 相機編輯頁
@IBDesignable class BackMenuWidget: Widgets {
    
    //MARK: - @IBAction
    @IBAction func didTapDoneButton(_ sender: UIButton) {
        let sb = UIStoryboard(name: "Landing", bundle: nil)
        if let vc = sb.instantiateInitialViewController() {
            UIApplication.shared.keyWindow?.rootViewController = vc
        }
    }
}

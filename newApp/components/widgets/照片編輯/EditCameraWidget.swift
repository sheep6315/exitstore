//
//  EditCameraWidget.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/16.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

protocol EditCameraDelegate {
    /// 重拍
    func retake()
    /// 上傳
    ///
    /// - Parameter image: 照片
    func upload(image:UIImage)
    /// 取消
    func exitStoreCancel()
}

/// 相機編輯頁
@IBDesignable class EditCameraWidget: Widgets {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var delegate:EditCameraDelegate?

    func initView(photo:UIImage?) {
        imageView.image = photo
        self.isHidden = false
    }
    
    //MARK: - @IBAction
    /// 照片上傳
    ///
    /// - Parameter sender:
    @IBAction func didTapUploadButton(_ sender: UIButton) {
        self.isHidden = true
        self.delegate?.upload(image: imageView.image!)
    }
    
    /// 重拍
    ///
    /// - Parameter sender:
    @IBAction func didTapRetakeButton(_ sender: UIButton) {
        self.isHidden = true
        self.delegate?.retake()
    }
    
    /// 取消
    ///
    /// - Parameter sender:
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        self.isHidden = true
        self.delegate?.exitStoreCancel()
    }
}

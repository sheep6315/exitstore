//
//  KeyboardUtility.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/12.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

protocol KeyboardUtilityDelegate {
    
    /// 當鍵盤隱藏時事件
    ///
    /// - Parameter sender: 隱藏來源
    func keyboardUtility(didDismiss sender: Any)
}

/// 鍵盤元件
class KeyboardUtility {
    
    static var delegate: KeyboardUtilityDelegate?
    
    /// 當前顯示頁面
    static var visibleView: UIView?
    
    /// 當前輸入元件
    static var currentResponder: UIView?
    
    /// 前一個輸入元件
    static var prevResponder: UIView?
    
    /// 後一個輸入元件
    static var nextResponder: UIView?
    
    /// 鍵盤外點擊手勢
    static var tapGesture: UITapGestureRecognizer?
    
    /// 原始畫面顯示定位
    static var contentInset: UIEdgeInsets?
    
    /// 原始畫面位移定位
    static var scrollIndicatorInsets: UIEdgeInsets?
    
    /// 初始化鍵盤事件
    class func initialize(withToolbar toolbar: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name:NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        if toolbar {
            UITextField.appearance().inputAccessoryView = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        }
    }
    
    class func cleanAllSetting() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    /// 設定鍵盤工具列按鈕清單
    private class func setToolbar() {
        var items: [UIBarButtonItem] = []
        if #available(iOS 11.0, *) {
            
        } else {
            let prevButton = UIBarButtonItem(title: "Prev", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.didTapPrevButton))
            
            prevButton.isEnabled = (self.prevResponder == nil) ? false : true
            items.append(prevButton)
            let nextButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.didTapNextButton))
            nextButton.isEnabled = (self.nextResponder == nil) ? false : true
            items.append(nextButton)
        }
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil))
        items.append(UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.dismissKeyboard(_:))))
        if items.count == 0 {
            return
        }
        var toolbarView: UIView?
        switch self.currentResponder {
        case nil:
            return
        case let textField as UITextField:
            toolbarView = textField.inputAccessoryView
        case let textView as UITextView:
            toolbarView = textView.inputAccessoryView
        default:
            AlertUtility.show(withMessage: "Unknow input type in KeyboardUtility(line:\(#line))", sender: nil)
            return
        }
        
        if let toolbar = toolbarView as? UIToolbar {
            toolbar.setItems(items, animated: true)
        }
    }
    
    /// 取消鍵盤工具列
    ///
    /// - Parameter sender: 顯示頁面
    class func disableToolbar(_ sender: UIView) {
        let fields = sender.collectionInputFields()
        for field in fields {
            switch field {
            case let textField as UITextField:
                textField.inputAccessoryView = nil
            case let textView as UITextView:
                textView.inputAccessoryView = nil
            default:
                AlertUtility.show(withMessage: "Unknow input type in KeyboardUtility(line:\(#line))", sender: nil)
                return
            }
        }
    }
    
    /// 取得當前顯示區塊
    ///
    /// - Parameter sender: 主區塊
    /// - Returns: 是否成功取得區塊
    class func findVisibleView(_ sender: UIView?) -> Bool {
        guard self.visibleView == nil else {
            return true
        }
        switch sender {
        case let scrollView as UIScrollView:
            if !scrollView.isHidden && scrollView.superview?.subviews.last == sender{
                self.visibleView = scrollView
                return true
            }
        case let tableView as UITableView:
            if !tableView.isHidden {
                self.visibleView = tableView
                return true
            }
        case _ as UITextField:
            return false
        case let searchBar as UISearchBar:
            if !searchBar.isHidden {
                self.visibleView = searchBar
                return true
            }
        default:
            break
        }
        for view in (sender?.subviews)! {
            if self.findVisibleView(view) {
                return true
            }
        }
        return false
    }
    
    /// 鍵盤顯示前，畫面上移
    ///
    /// - Parameter notification: 通知內容
    @objc class func keyboardWillShow(_ notification: Notification) {
        guard let info = (notification as NSNotification?)?.userInfo else {
            return
        }
        
        if self.findVisibleView(UIApplication.topViewController()?.view) != true {
            self.visibleView = UIApplication.topViewController()?.view
        }
        self.setResponders()
        
        guard (self.currentResponder != nil) else {
            return
        }
        
        self.setToolbar()
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let bottom: CGFloat = keyboardFrame.size.height
        
        switch self.visibleView {
        case let scrollView as UIScrollView:
            if self.contentInset == nil && self.scrollIndicatorInsets == nil {
                self.contentInset = scrollView.contentInset
                self.scrollIndicatorInsets = scrollView.scrollIndicatorInsets
            }
            let positionY = scrollView.contentInset.top
            let indicatorY = scrollView.scrollIndicatorInsets.top
            scrollView.contentInset = UIEdgeInsets(top: positionY, left: 0, bottom: bottom + 20, right: 0)
            scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(indicatorY, 0, bottom, 0)
        case let tableView as UITableView:
            if self.contentInset == nil && self.scrollIndicatorInsets == nil {
                self.contentInset = tableView.contentInset
                self.scrollIndicatorInsets = tableView.scrollIndicatorInsets
            }
            self.contentInset = tableView.contentInset
            self.scrollIndicatorInsets = tableView.scrollIndicatorInsets
            let positionY = tableView.contentInset.top
            let indicatorY = tableView.scrollIndicatorInsets.top
            tableView.contentInset = UIEdgeInsets(top: positionY, left: 0, bottom: bottom + 20, right: 0)
            tableView.scrollIndicatorInsets = UIEdgeInsetsMake(indicatorY, 0, bottom, 0)
        default:
            self.contentInset = UIEdgeInsets(top: (self.visibleView?.frame.origin.y)!, left: 0, bottom: 0, right: 0)
            let screenHeight = UIScreen.main.bounds.height
            let positionY = self.currentResponder?.frame.origin.y
            let scrollPosition = (screenHeight - positionY!)
            if scrollPosition <= bottom {
                self.visibleView?.frame.origin.y -= bottom - 100
            }
        }
    }
    
    /// 鍵盤顯示，顯示工具列
    ///
    /// - Parameter notification: 通知內容
    @objc class func keyboardDidShow(_ notification: Notification) {
        self.setResponders()
        self.setToolbar()
        self.visibleView?.addGestureRecognizer(self.tapGesture!)
    }
    
    /// 鍵盤隱藏，畫面移回原位
    ///
    /// - Parameter notification: 通知內容
    @objc class func keyboardWillHide(_ notification: Notification) {
        
        self.visibleView?.removeGestureRecognizer(self.tapGesture!)
        switch self.visibleView {
        case let scrollView as UIScrollView:
            if self.contentInset != nil && self.scrollIndicatorInsets != nil {
                scrollView.contentInset = self.contentInset!
                scrollView.scrollIndicatorInsets = self.scrollIndicatorInsets!
            }
        case let tableView as UITableView:
            if self.contentInset != nil && self.scrollIndicatorInsets != nil {
                tableView.contentInset = self.contentInset!
                tableView.scrollIndicatorInsets = self.scrollIndicatorInsets!
            }
        default:
            if self.contentInset != nil && self.scrollIndicatorInsets != nil {
                self.visibleView?.frame.origin.y = (self.contentInset?.top)!
            }
        }
        self.contentInset = nil
        self.scrollIndicatorInsets = nil
        self.visibleView = nil
        self.currentResponder = nil
    }
    
    class func setResponders() {
        let responders = self.visibleView?.findResponder()
        self.currentResponder = responders?.current
        self.prevResponder = responders?.prev
        self.nextResponder = responders?.next
    }
    
    /// 點擊上一個按鈕動作
    @objc class func didTapPrevButton() {
        self.prevResponder?.becomeFirstResponder()
    }
    
    /// 點擊下一個按鈕動作
    @objc class func didTapNextButton() {
        self.nextResponder?.becomeFirstResponder()
    }
    
    /// 隱藏鍵盤
    ///
    /// - Parameter sender: 隱藏來源
    @objc class func dismissKeyboard(_ sender: Any) {
        if let searchBar = visibleView as? UISearchBar {
            searchBar.resignFirstResponder()
        }
        
        self.visibleView?.endEditing(true)
        
        if self.currentResponder != nil {
            var toolbarView: UIView?
            switch self.currentResponder {
            case nil:
                return
            case let textField as UITextField:
                toolbarView = textField.inputAccessoryView
            case let textView as UITextView:
                toolbarView = textView.inputAccessoryView
            default:
                AlertUtility.show(withMessage: "Unknow input type in KeyboardUtility(line:\(#line))", sender: nil)
                return
            }
            if let toolbar = toolbarView as? UIToolbar {
                toolbar.setItems([], animated: false)
            }
        }
        self.visibleView = nil
        self.delegate?.keyboardUtility(didDismiss: sender)
    }
}

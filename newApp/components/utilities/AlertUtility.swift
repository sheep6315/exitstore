//
//  AlertUtility.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

/// 提示訊息工具
struct AlertUtility {
    
    /// 顯示提示訊息
    ///
    /// - Parameters:
    ///   - message: 訊息內容
    ///   - sender: 顯示來源
    ///   - actionHandler: 點擊確認按鈕後動作
    static func show(withMessage message: String, sender: UIViewController?, actionHandler: ((UIAlertAction) -> Void)? = nil) {
        
        var messageLocalized:String = message
        if let vc = sender {
            messageLocalized = message
            vc.view.endEditing(true)
        }
        
        let alertController = UIAlertController(title: nil, message: messageLocalized, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alertAction) in
            actionHandler?(alertAction)
        }))
        let presentViewController: UIViewController = sender ?? UIApplication.topViewController()!
        presentViewController.present(alertController, animated: true, completion: nil)
    }
    
    /// 顯示提示訊息及自定義按鈕
    ///
    /// - Parameters:
    ///   - message: 訊息內容
    ///   - sender: 顯示來源
    ///   - actions: 按鈕清單
    static func showWithActions(withMessage message: String, sender: UIViewController?, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addActions(actions: actions)
        
        let presentViewController: UIViewController = sender ?? UIApplication.topViewController()!
        presentViewController.present(alertController, animated: true, completion: nil)
    }
}

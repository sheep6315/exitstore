//
//  TSTAR_API.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit
import CryptoSwift

let API_CACHE_TIME: Double = 1800

let API_CACHED_DATA = "API_CACHED_DATA"

let aesKey: String = "09084rfv5tgb6yhn"
let aesIv: String  = "09084rfv5tgb6yhn"

enum StatusCode:String {
    case success = "success"
    case fail = "fail"
    init(fromRawValue: String){
        self = StatusCode(rawValue: fromRawValue) ?? .fail
    }
}

enum HttpMethod:String {
    case POST
    case GET
}

class TSTAR_API: NSObject {
    
    static let sharedInstance = TSTAR_API()
    
    var tasks: [String: [Int]] = [:]
    
    private override init() {
        //This prevents others from using the default '()' initializer for this class.
    }
    
    func startTask(withURLString urlString: String,httpMethod:HttpMethod, data: Data, showProgress: Bool, progressMessage: String?, cacheTime: Double, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = URL(string: encodedUrlString!)
        
        if let url = url {
            
            if cacheTime > 0 {
                let cachedDic = returnCachedData(ofName: urlString, inSecond: cacheTime)
                
                guard cachedDic == nil else {
                    if showProgress {
                        HudUtility.hideProgress()
                    }
                    completion(cachedDic!,.success,cachedDic!.stringValue(ofKey: "errmsg"))
                    return
                }
            }
            if showProgress {
                HudUtility.showProgress(title: nil, message: progressMessage)
            }
            let task = self.createTask(withURL: url, httpMethod: httpMethod, cacheDicName: urlString, data: data, showProgress: showProgress, completion: completion, failure: failure)
            
            DispatchQueue.main.async {
                let vcName = String(describing: type(of: UIApplication.topViewController()!))
                if var taskPool = self.tasks[vcName] {
                    taskPool.append(task.taskIdentifier)
                    self.tasks[vcName] = taskPool
                } else {
                    self.tasks[vcName] = [task.taskIdentifier]
                }
                task.resume()
            }
        } else {
            if showProgress {
                HudUtility.hideProgress()
            }
            failure(NSError(domain: "Not valid URL", code: 1, userInfo: nil))
        }
    }
    
    /// 啟動任務
    ///
    /// - Parameters:
    ///   - urlString: url網址
    ///   - data: 資料
    ///   - showProgress: 是否顯示等待視圖
    ///   - completion: 完成時執行動作
    ///   - failure: 失敗時執行動作
    internal func startTask(withURLString urlString: String,httpMethod:HttpMethod, dic: [String:Any], showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        startTask(withURLString: urlString,httpMethod: httpMethod, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, progressMessage: nil, cacheTime: 0, completion: completion, failure: failure)
    }
    
    /// 啟動任務（含cacheTime）
    ///
    /// - Parameters:
    ///   - urlString: url網址
    ///   - data: 資料
    ///   - showProgress: 是否顯示等待視圖
    ///   - cacheTime: 資料暫存時間
    ///   - completion: 完成時執行動作
    ///   - failure: 失敗時執行動作
    internal func startCacheTask(withURLString urlString: String,httpMethod:HttpMethod, dic: [String:Any], showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        startTask(withURLString: urlString,httpMethod: httpMethod, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, progressMessage: nil, cacheTime: API_CACHE_TIME, completion: completion, failure: failure)
        
    }
    
    private func createTask(withURL url: URL,httpMethod:HttpMethod,cacheDicName:String, data: Data, showProgress: Bool, completion: @escaping (_ result:Dictionary<String, Any>,_ statusCode:StatusCode,_ message: String) -> Void, failure: @escaping (Error) -> Void) -> URLSessionTask {
        
        var request: URLRequest = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: API_TIMEOUT_INTERVAL)
        request.httpBody = data
        request.httpMethod = httpMethod.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session: URLSession = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            
        DispatchQueue.main.async {
            if showProgress {
                HudUtility.hideProgress()
            }
            
            let dic = self.dictionary(fromResponseData: data)
            
            if error == nil {
                print("\nTSTAR_API \(url.absoluteString)\n\(dic.description)")
                
                let statusCode = StatusCode(fromRawValue: dic.stringValue(ofKey: "result"))
                if statusCode == .success {
                    self.saveDicFromAPI(name: cacheDicName, dic: dic)
                }
                let message = dic.stringValue(ofKey: "errmsg")
                completion(dic,statusCode,message)
            }
            else {
                print("\nTSTAR_API \(url.absoluteString)\n\(error!.localizedDescription)")
                if let err = error as NSError? {
                    switch err.code {
                    case NSURLErrorTimedOut,NSURLErrorNotConnectedToInternet:
                        failure(error!)
                    case NSURLErrorCancelled:
                        break
                    default:
                        failure(error!)
                    }
                }else {
                    failure(error!)
                }
            }
        }
        
        
    })
        
        return task
    }
    
    private func syncTask(withURLString urlString: String, data: Data, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = URL(string: encodedUrlString!)
        var request: URLRequest = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: API_TIMEOUT_INTERVAL)
        request.httpBody = data
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session: URLSession = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            
            let dic = self.dictionary(fromResponseData: data)
            
            if error == nil {
                print("\nTSTAR_API \(String(describing: url?.absoluteString))\n\(dic.description)")
                completion(dic, response!)
            } else {
                print("\nTSTAR_API \(String(describing: url?.absoluteString))\n\(error!.localizedDescription)")
                failure(error!)
            }
            semaphore.signal()
        })
        task.resume()
        semaphore.wait()
    }
    
    /**
     Parse JSON Data into Dictionary<String, Any>
     */
    private func dictionary(fromResponseData data: Data?) -> Dictionary<String, Any> {
        var resultDic: Dictionary<String, Any> = [:]
        
        if data != nil {
//            var rawString = String(data: data!, encoding: String.Encoding.utf8)
            do {
                resultDic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! Dictionary<String, Any>
//                print("resultDic \(resultDic.description)")
            } catch let parseError {
                print("parseError \(parseError.localizedDescription)")
            }
            
        }
        
        return resultDic
    }
    
    internal func getEncryptedData(ofDictionary dic: Dictionary<String, Any>) -> Data {
        var jsonData = Data()
        do {
            let dicData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString = String.init(data: dicData, encoding: String.Encoding.utf8)!

            let requestDic = [
                "data": getAESEncrypt(withString: jsonString)
            ]
            
            jsonData = try JSONSerialization.data(withJSONObject: requestDic, options: JSONSerialization.WritingOptions(rawValue: 0))
            
        } catch let error {
            print("encryptedData \(error.localizedDescription)")
        }
        
        return jsonData
    }
    
    private func getEncryptedDataForBSCAPI(ofDictionary dic: Dictionary<String, Any>) -> Data {
        var jsonData = Data()
        do {
            let dicData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString = String.init(data: dicData, encoding: String.Encoding.utf8)!
            
            let requestDic = ["request": getAESEncrypt(withString: jsonString)]
            jsonData = try JSONSerialization.data(withJSONObject: requestDic, options: JSONSerialization.WritingOptions(rawValue: 0))
        } catch let error {
            print("encryptedData \(error.localizedDescription)")
        }
        return jsonData
    }
    
    internal func getAESEncrypt(withString originString: String) -> String {
        var result = ""
        do {
            let aesKeyArray: Array<UInt8> = TSTAR_API.sharedInstance.aesEncryptKeyArray()
            let encryptedData = try AES(key: aesKeyArray, blockMode: BlockMode.CBC(iv: aesKeyArray), padding: Padding.pkcs7).encrypt(Array(originString.utf8))
            result = encryptedData.toBase64()!
        } catch let error {
            print("aesEncrypt \(error.localizedDescription)")
        }
        
        return result
    }
    
    
    /// 取得解密資料
    ///
    /// - Parameter message: 訊息
    /// - Returns: Dictionary<String, Any>?
    func getDecryptDic(_ message: String) -> Dictionary<String, Any>? {
        do {
            if let base64Data = Data(base64Encoded: message) {
                let encrypted = base64Data.bytes
                let descrptData = try AES(key: aesKey, iv: aesIv, padding: Padding.pkcs7).decrypt(encrypted)
                
                return String(bytes: descrptData, encoding: .utf8)?.jsonDecode()
            }
        } catch let error {
            print("aesDecrypt \(error.localizedDescription)")
        }
        return nil
    }
    
    internal func aesEncryptKey() -> String {
        let originKey = "a0a9a0a8a4arafava5atagaba6ayahan"
        var key = ""
        for index in 1...originKey.lengthOfBytes(using: String.Encoding.utf8) {
            if (index % 2) == 1 {
                let char: Character = originKey[originKey.index(originKey.startIndex, offsetBy: index)]
                key.append(char)
            }
        }
        
        return key
    }
    
    internal func aesEncryptKeyArray() -> Array<UInt8> {
        let originKey = "a0a9a0a8a4arafava5atagaba6ayahan"
        var keyArray: Array<UInt8> = []
        var key = ""
        
        for index in 1...originKey.lengthOfBytes(using: String.Encoding.utf8) {
            if (index % 2) == 1 {
                let char: Character = originKey[originKey.index(originKey.startIndex, offsetBy: index)]
                key.append(char)
            }
        }
        keyArray = key.bytes
        
        return keyArray
    }
    
    private func getXmlString(ofDictionary dic: Dictionary<String, String>) -> String {
        var xmlString = ""
        dic.forEach { (raw:(key: String, value: String)) in
            xmlString += "<\(raw.key)>\(raw.value)</\(raw.key)>"
        }
        return xmlString
    }
    
    private func saveDicFromAPI(name: String, dic: Dictionary<String, Any>) {
        
        if UserDefaults.standard.dictionary(forKey: API_CACHED_DATA) == nil {
            UserDefaults.standard.set([:], forKey: API_CACHED_DATA)
        }
        
        let timeStamp = Date().timeIntervalSince1970
        let apiResponseData = ["time": timeStamp, "dic": dic] as [String : Any]
        
        var cachedData = UserDefaults.standard.dictionary(forKey: API_CACHED_DATA)! as Dictionary<String, Any>
        cachedData[name] = NSKeyedArchiver.archivedData(withRootObject: apiResponseData).base64EncodedString()
        UserDefaults.standard.set(cachedData, forKey: API_CACHED_DATA)
        UserDefaults.standard.synchronize()
    }
    
    private func returnCachedData(ofName name: String, inSecond second: Double) -> Dictionary<String, Any>? {
        
        guard UserDefaults.standard.dictionary(forKey: API_CACHED_DATA) != nil else {
            return nil
        }
        
        var dic: Dictionary<String, Any>? = nil
        let cachedData = UserDefaults.standard.dictionary(forKey: API_CACHED_DATA)! as Dictionary<String, Any>
        
        if cachedData[name] != nil {
            let timeStamp = Date().timeIntervalSince1970
            
            let cachedAPIResponseDataInBase64String = cachedData[name] as! String
            let cachedAPIResponseData = NSKeyedUnarchiver.unarchiveObject(with: Data(base64Encoded: cachedAPIResponseDataInBase64String)!) as! [String: Any]
            let cachedTime = cachedAPIResponseData["time"] as! TimeInterval
            
            
            let interval = round(timeStamp - cachedTime)
            
            if interval < second {
                dic = cachedAPIResponseData["dic"] as? Dictionary<String, Any>
//                print("cached dic \(String(describing: dic!.description))")
                print("cached dic \(dic!.description)")
            }
        }
        
        return dic
    }
    
    //MARK: - API
    //MARK: - 離店打卡
    func checkIn(date: String, reason: String, description: String, showProgress: Bool, completion: @escaping (_ result:Dictionary<String, Any>,StatusCode,_ message:String) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "ad_account": " yenluntseng",
            "empId": " 18079152",
            "cname": "曾*綸",
            "dealercode": "3301009",
            "dealertitle": "新竹中央",
            "drtime": " 2018-08-15 16:30:00",
            "reason": "5",
            "description": "至XX公司拜訪客戶",
            "status": "1",
            "geolocation": [
                "lat": "24.808523",
                "lon": "120.9736"
            ]
        ] as [String : Any]
        
        let urlString = "\(SERVER_HOST)"
        
        print("TSTAR_API checkIn\n\(dic.description)")

//        self.startTask(withURLString: urlString,dic: dic, showProgress: showProgress, completion: completion, failure: failure)
    }
    
    //MARK: - 上傳照片
    /// 上傳照片
    ///
    /// - Parameters:
    ///   - picFileBinary: 照片二位元
    ///   - showProgress: 是否顯示等待視圖
    ///   - completion: 完成時執行動作
    ///   - failure: 失敗時執行動作
    func uploadPhoto(withPicFileBinary picFileBinary: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "type": "UPDATEMEMBERINFO",
//            "ContractId" : UserModel.shared!.CONTRACTID,
            "UD_COLUMN" : "PICFILEBINARY",
            "UD_VALUE1" : picFileBinary,
//            "UD_VALUE2" : "\(UserModel.shared!.UID).jpg"
            ] as [String : Any]
        
        print("TSTAR_API uploadPhoto\n\(dic.description)")
        completion(dic,StatusCode.success,"")
//        self.startTask(withURLString: SERVER_HOST,dic: dic, showProgress: showProgress, completion: completion, failure: failure)
    }
    
    //MARK: - 登入
    func login(withMSISDN msisdn: String, password: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let urlString = "\(SERVER_HOST)/auth/login"
        
        let dic = [
            "username" : msisdn,
            "password" : password
            ] as [String : Any]
        
        print("TSTAR_API login\n\(dic.description)")

        self.startTask(withURLString: urlString, httpMethod: .POST, dic: dic, showProgress: true, completion: completion, failure: failure)
    }
    
    //MARK: - 查詢 版本資訊
    func getAppVersion(showProgress: Bool, completion: @escaping (Dictionary<String, Any>,StatusCode,String) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "type" : "GETAPPVERSION",
            "MSISDN" : "0",
            "APP_VRID" : "\(Bundle.main.releaseVersionNumber ?? "")",
            "APP_OS" : "I"
            ] as [String : Any]
        
        print("TSTAR_API getAppVersion\n\(dic.description)")
        self.startTask(withURLString: SERVER_HOST,httpMethod:.POST,dic: dic, showProgress: showProgress, completion: completion, failure: failure)
    }
}

//
//  ThemesUtility.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit

/// 版面主題工具
struct ThemesUtility {
    
    /// 設定預設主題樣式
    static func setDefaultStyle() {
        self.setStatusBar(withColor: DEFAULT_STATUS_BAR_COLOR)
        self.setNavigationBarStyle(withBackgroundColor: DEFAULT_NAVIGATION_BAR_BACKGROUND_COLOR, titleColor: DEFAULT_NAVIGATION_BAR_TITLE_COLOR)
    }
    
    /// 設定status bar顏色
    ///
    /// - Parameter color: 背景顏色
    static func setStatusBar(withColor color: UIColor) {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if UIDevice.isIphoneX {
            UIApplication.shared.statusBarStyle = .lightContent
            statusBar.backgroundColor = UIColor.black
        } else {
            UIApplication.shared.statusBarStyle = DEFAULT_STATUS_BAR_STYLE
            statusBar.backgroundColor = color
        }
    }
    
    /// 設定navagation bar顏色
    ///
    /// - Parameters:
    ///   - bgColor: 背景顏色
    ///   - titleColor: 顏色
    static func setNavigationBarStyle(withBackgroundColor bgColor: UIColor, titleColor: UIColor) {
        UINavigationBar.appearance().barTintColor = bgColor
        UINavigationBar.appearance().tintColor = titleColor
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: titleColor]
    }
}

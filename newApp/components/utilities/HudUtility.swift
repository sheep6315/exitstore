//
//  HudUtility.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import PKHUD

/// HUD工具
struct HudUtility {
    
    /// 顯示Activity Indicator
    ///
    /// - Parameters:
    ///   - title: 標題
    ///   - message: 內容
    ///   - mask: 是否顯示背景遮罩
    static func showProgress(title: String?, message: String?, mask: Bool  = true) {
        DispatchQueue.main.async {
            PKHUD.sharedHUD.dimsBackground = mask
            PKHUD.sharedHUD.contentView = PKHUDProgressView(title: title, subtitle: message)
            PKHUD.sharedHUD.show()
        }
    }
    
    /// 關閉Activity Indicator
    static func hideProgress() {
        DispatchQueue.main.async {
            PKHUD.sharedHUD.hide()
        }
    }
}

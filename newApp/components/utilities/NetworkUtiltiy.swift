//
//  NetworkUtiltiy.swift
//  newApp
//
//  Created by 詮通電腦-江宗澤 on 2018/7/9.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//

import UIKit
import SystemConfiguration
import CoreTelephony

/// 網路工具
struct NetworkUtiltiy {
    
    /// 網路連線狀態
    ///
    /// - DISCONNECT: 無網路連線
    /// - WIFI: WIFI
    /// - TYPE_4G: 4G
    /// - TYPE_3G: 3G
    /// - TYPE_2G: 2G
    /// - UNKNOW: 其他網路
    enum EnumNetworkStatus {
        case DISCONNECT, WIFI, TYPE_4G, TYPE_3G, TYPE_2G, UNKNOW
    }
    
    /// HTTP連線狀態
    ///
    /// - SUCCESS: 請求成功
    /// - FORBIDDEN: 無訪問權限
    /// - NOT_FOUND: 功能不存在
    /// - TIMEOUT: 連線超時
    /// - SERVER_FAULT: 伺服器異常
    /// - UNDEFINED: 不明錯誤
    private enum EnumHttpStatus: Int, Error {
        case SUCCESS = 200
        case FORBIDDEN = 403
        case NOT_FOUND = 404
        case TIMEOUT = 408
        case SERVER_FAULT = 500
        case UNDEFINED = 999
    }
    
    /// 網路路由可用性
    private static var routeReachability: SCNetworkReachability? {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1, {
                zeroSocketAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSocketAddress)
            })
        }
        return defaultRouteReachability
    }
    
    /// 可用性標示
    private static var reachabilityFlags: SCNetworkReachabilityFlags {
        var flags = SCNetworkReachabilityFlags(rawValue: 0)
        if !SCNetworkReachabilityGetFlags(self.routeReachability!, &flags) {
            flags = []
        }
        return flags
    }
    
    /// 網路連線狀態
    static var isConnected: Bool {
        var flags = self.reachabilityFlags
        if !SCNetworkReachabilityGetFlags(self.routeReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
    }
    
    /// 網路連線類型
    static var connectionType: EnumNetworkStatus {
        if self.isConnected {
            if self.reachabilityFlags.contains(.isWWAN) {
                let telephoneyInfo: CTTelephonyNetworkInfo = CTTelephonyNetworkInfo()
                
                switch telephoneyInfo.currentRadioAccessTechnology! {
                case CTRadioAccessTechnologyLTE:
                    return .TYPE_4G
                case CTRadioAccessTechnologyWCDMA, CTRadioAccessTechnologyeHRPD, CTRadioAccessTechnologyHSDPA, CTRadioAccessTechnologyHSUPA, CTRadioAccessTechnologyCDMAEVDORev0, CTRadioAccessTechnologyCDMAEVDORevA, CTRadioAccessTechnologyCDMAEVDORevB:
                    return .TYPE_3G
                case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
                    return .TYPE_2G
                default:
                    return .UNKNOW
                }
            } else {
                return .WIFI
            }
        } else {
            return .DISCONNECT
        }
    }
    
    /// Call web service API
    ///
    /// - Parameters:
    ///   - urlRequest: API request info
    ///   - async: 是否使用異步傳輸模式
    ///   - showProgress: 是否顯示等待視圖
    ///   - priority: 任務權重
    ///   - completion: API完成時執行動作
    ///   - failure: API失敗時執行動作
    static func webServiceTask(withUrlReuqest urlRequest: URLRequest, async: Bool, showProgress: Bool, priority: Float = URLSessionTask.defaultPriority, completion: ((HTTPURLResponse, Data) -> Void)? = nil, failure: ((HTTPURLResponse?, Error?) -> Void)? = nil) {
        
        guard self.isConnected else {
            AlertUtility.showWithActions(withMessage: "Disconnected", sender: nil, actions: [
                UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
                    failure?(nil, nil)
                }),
                UIAlertAction(title: "Retry", style: .default, handler: { (_) in
                    self.webServiceTask(withUrlReuqest: urlRequest, async: async, showProgress: showProgress, priority: priority, completion: completion, failure: failure)
                })
                ]
            )
            return
        }
        
        if showProgress {
            HudUtility.showProgress(title: nil, message: nil)
        }
        
        var request = urlRequest
        request.cachePolicy = .reloadIgnoringLocalCacheData
        request.timeoutInterval = API_TIMEOUT_INTERVAL
        
        let session = URLSession.shared
        
        let semaphore = DispatchSemaphore(value: 0)
        let task = session.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            DispatchQueue.main.async {
                if showProgress {
                    HudUtility.hideProgress()
                }
            }
            if let errorInfo = error as NSError? {
                switch errorInfo.code {
                case NSURLErrorCancelled:
                    return
                default:
                    break
                }
            } else if let apiResponse = response as? HTTPURLResponse {
                if error == nil && apiResponse.statusCode == EnumHttpStatus.SUCCESS.rawValue {
                    completion?(apiResponse, data!)
                } else {
                    var errorMsg: String = "UNDEFINED"
                    
                    if error == nil {
                        let errorType: EnumHttpStatus? = EnumHttpStatus(rawValue: apiResponse.statusCode)
                        switch errorType {
                        case .FORBIDDEN?:
                            errorMsg = "FORBIDDEN"
                            break
                        case .NOT_FOUND?:
                            errorMsg = "NOT_FOUND"
                            break
                        case .TIMEOUT?:
                            errorMsg = "TIMEOUT"
                            break
                        case .SERVER_FAULT?:
                            errorMsg = "SERVER_FAULT"
                            break
                        default:
                            break
                        }
                    }
                    
                    AlertUtility.show(withMessage: errorMsg, sender: nil, actionHandler: { (_) in
                        failure?(apiResponse, error)
                    })
                }
            }
            semaphore.signal()
        }
        task.priority = priority
        task.resume()
        if !async {
            semaphore.wait()
        }
    }
    
    /// 停止全部低權重任務
    ///
    /// - Parameter priority: 權重
    static func cancelAllTask(vc:UIViewController = UIApplication.topViewController()!,priorityLowerThan priority: Float = URLSessionTask.highPriority) {

        URLSession.shared.getAllTasks { (tasks) in
            if tasks.count > 0 {
                tasks.forEach({ (task) in
                    DispatchQueue.main.async(execute: {
                        let vcName = String(describing: type(of: vc))
                        if let taskPool = TSTAR_API.sharedInstance.tasks[vcName] {
                            if taskPool.contains(task.taskIdentifier) {
                                if task.priority < priority {
                                    task.cancel()
                                }
                            }
                            TSTAR_API.sharedInstance.tasks.removeValue(forKey: vcName)
                        }
                    })
                })
            }
        }
    }
}

//
//  TStarWebViewController.swift
//  DEV
//
//  Created by 詮通電腦-江宗澤 on 2018/8/10.
//  Copyright © 2018年 詮通電腦-江宗澤. All rights reserved.
//
import UIKit

class TStarWebViewController: BaseViewController, UIWebViewDelegate {
    
    @IBOutlet weak var progressBar: UIProgressView!
    
    @IBOutlet weak var contentWebView: UIWebView!
    
    @IBOutlet weak var toolBar: UIToolbar!
    
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    @IBOutlet weak var forwardButton: UIBarButtonItem!
    
    @IBOutlet weak var stopButton: UIBarButtonItem!
    
    @IBOutlet weak var reloadButton: UIBarButtonItem!
    
    @IBOutlet weak var progressViewHeightConstraint: NSLayoutConstraint!
    
    var webUrl: String! = ""
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.automaticallyAdjustsScrollViewInsets = true
        
        self.progressBar.setProgress(0, animated: false)
        self.progressViewHeightConstraint.constant = 0
        self.view.setNeedsUpdateConstraints()
        self.view.layoutIfNeeded()
        self.contentWebView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.title != nil {
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
        }
        
        preprocessURL()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isTranslucent = false
        self.tabBarController?.tabBar.isHidden = false
        contentWebView.stopLoading()
    }
    
    private func toggleToolButton() {
        self.backButton.isEnabled = self.contentWebView.canGoBack
        self.forwardButton.isEnabled = self.contentWebView.canGoForward
    }
    
    // MARK: - UIWebViewDelegate
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.progressBar.setProgress(0.6, animated: true)
        self.stopButton.isEnabled = true
        self.reloadButton.isEnabled = false
        self.progressViewHeightConstraint.constant = 2
        self.view.setNeedsUpdateConstraints()
        self.view.layoutIfNeeded()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.progressBar.setProgress(1, animated: true)
        self.stopButton.isEnabled = false
        self.reloadButton.isEnabled = true
        self.progressViewHeightConstraint.constant = 0
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 1, animations: {
            self.view.layoutIfNeeded()
        }, completion:  { (isComplete: Bool) in
            self.progressBar.setProgress(0, animated: false)
        })
        self.toggleToolButton()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("error: ",error)
        self.progressBar.setProgress(1, animated: true)
        self.stopButton.isEnabled = false
        self.reloadButton.isEnabled = true
        self.progressViewHeightConstraint.constant = 0
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 1, animations: {
            self.view.layoutIfNeeded()
        }, completion:  { (isComplete: Bool) in
            self.progressBar.setProgress(0, animated: false)
        })
        self.toggleToolButton()
    }
    
    // MARK: - IBAction
    @IBAction func didTapBack(_ sender: Any) {
        self.contentWebView.goBack()
        self.toggleToolButton()
    }
    
    @IBAction func didTapForward(_ sender: Any) {
        self.contentWebView.goForward()
        self.toggleToolButton()
    }
    
    @IBAction func didTapStop(_ sender: Any) {
        self.contentWebView.stopLoading()
        self.toggleToolButton()
    }
    
    @IBAction func didTapReload(_ sender: Any) {
        self.contentWebView.reload()
        self.toggleToolButton()
    }
    
    @IBAction func didTapAction(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "使用Safari開啟", style: .default, handler: { (_: UIAlertAction) in
            //            if let url = URL(string: self.webUrl) {
            //                UIApplication.shared.openURL(url)
            //            }
            UIApplication.shared.openURL((self.contentWebView.request?.url)!)
        }))
        alertController.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        //        UIApplication.shared.keyWindow?.rootViewController?.present(actionAlertController, animated: true, completion: nil)
        
        alertController.show(viewController: self)
    }
    
    //    MARK: - Preprocess URL
    func preprocessURL() {
        DispatchQueue.main.async {
            
            self.webUrl = self.webUrl.replacingOccurrences(of: " ", with: "")
            
            if let url = URL(string: self.webUrl) {
                let scheme = url.scheme
                let host = url.host
                let request = URLRequest(url: url)
                self.contentWebView.loadRequest(request)
            }
        }
    }
    
}
